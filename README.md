# CSIT 406: Server-Side Web Application Development
This course is designed to assist students in learning the skills necessary to design and build Internet-based information systems. Skills and knowledge gained in this course can be applied in the development of information systems that support interactive Web sites, electronic commerce systems, and other systems that involve interaction with a database through the Internet. Security of Internet based information systems will also be covered.

## Overview
>This course will teach enterprise web application architecture concepts using a technology stack in wide use today.  Some techniques/tools will be covered directly in class, but others may require outside learning.  It is important that students read all assigned material. Students should work hard to understand the tools and techniques introduced in this course.

## Using Class Documentation

> ### README Files
> Assigned reading, installation instructions, and references can be found in the README files (file extension .md) contained within this repository.  With the exception of this page all README files can be found in the docs/readme directory.  These files may be viewed as html in [Bitbucket](https://bitbucket.org/bealoper/csit-406-spring18-prep), by pasting the contents into an online viewer such as [Markdown Live Preview](http://markdownlivepreview.com/), or by installing a [markdown viewer plugin](https://github.com/satyagraha/gfm_viewer) for your favorite IDE.

> ### In Code Documentation
> Explanation of the code examples we do in class will be in the form of standard Java line and block comments.  Class and method comment blocks will be in the standard [Javadoc](http://www.oracle.com/technetwork/articles/java/index-137868.html) format.


## Sections
> * 01 [Development Tools](docs/readme/01_DEVTOOLS.md)
> * 02 [Architecture](docs/readme/02_ARCHITECTURE.md)
> * 03 [Testing](docs/readme/03_TESTING.md)
> * 04 [Persistence](docs/readme/04_PERSISTENCE.md)
> * 05 [Services](docs/readme/05_SERVICES.md)
> * 06 [Web UI](docs/readme/06_WEBUI.md)
> * 07 [Web API](docs/readme/07_WEBAPI.md)
> * 08 [Security](docs/readme/08_SECURITY.md)
> * 09 [Deployment](docs/readme/09_DEPLOYMENT.md)

