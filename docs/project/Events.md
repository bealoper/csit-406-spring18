#  Events

## Models
> - **Event**
> 	- id pk
>	- label
>	- description
> 	- location_id
>	- profile_id
>	- startDateTime
>	- duration
>	- attendees (Collection)
>	- comments (Collection of comment ids)
>	- likes(Collection of UserId)
>	- recurrence (if we can get it in)

> - **Attendee**
>	- Type (Enum Invited, RSVP, Attended)