# Locations

## Models
> - **Location**
>	- id pk
>	- label
>	- description
>	- address
>	- latitude
>	- longitude
>	- (S2 or zip depending on how we move forward)

> - **Address**
>	- address1
>	- address2
>	- city
>	- state
>	- zipcode