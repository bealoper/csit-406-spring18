# Maven Artifacts

## Content
<groupId>edu.unk.csit406</groupId>
<artifactId>content</artifactId>
<version>1.0.0</version>

## Region *
<groupId>edu.unk.csit406</groupId>
<artifactId>Region</artifactId>
<version>1.0.0</version>

# Project User *
<groupId>edu.unk.cs406.user</groupId>
<artifactId>project-user</artifactId>
<version>1.0.0</version>

# Project Search
<groupId>edu.unk.morseka</groupId>
<artifactId>Project-Search</artifactId>
<version>1.0.0</version>

## Event *
<groupId>edu.unk.csit406.event</groupId>
<artifactId>event-project</artifactId>
<version>1.0.0</version>

## Location & Comments *
<groupId>edu.unk.geweckear</groupId>
<artifactId>location</artifactId>
<version>1.1.0</version>

