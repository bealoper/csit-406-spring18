# Project Modules

## Region (Emma)
> * Parent to UserGroup, UserProfile, Business/Venue
> * Need to decide how regions are defined (manual/automatic) and how children are assigned.

## Profile: UserGroup (Eduardo)
> * See Tree Below
> * May have features that require moderation
>	- Membership
>	- Public/Private
>	- Comments/Likes

## Profile: UserProfile (Sujan)
> * See Tree Below

## Profile: Business/Venue (Prabin)
> * See Tree Below
> * May need some moderation

## Search (Colton)
> * All profiles register and all events and all locations
> * Has searchable tags
> * Needs some way to rank.

## Events (Jacob)
> * Store event objects 
> * Display events (calendar etc)
> * Use Location API to connect to physical locations
> * Use Comments API
> * Create a RSVP structure for the event

## Location (Abby)
> * Store physcial locations and some minimal data to describe location in map api (i.e. Google Maps)
> * Capture physical locations from end users.

## Comments (Abby)
> * Generic service for collecting and displaying comments


# Research
> * Eduardo looking for frameworks or plugins for notifications/messaging
> * Abby will need to research Map APIs to collect and display locations
> * Jacob will need to decide how involved to make events (calendar api stuff)


# Profile Common Structures
> * Profile
>	- List of Comments
>	- List of Events
>	- List of Locations
>	- Aggregate Likes
>	- List of Favorites