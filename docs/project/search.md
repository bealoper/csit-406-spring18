# Search

## Models
> - **Entity** 
>	- id pk
>	- label
>	- type (enum Region, Profile, Location, Event)
>	- latitude
>	- longitude
>	- possible (s2 or zipcode depending on location service)
>	- tags (collection of string)
>- 	- startTime (if practical)