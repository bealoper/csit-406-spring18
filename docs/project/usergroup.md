# Profile User Group

## Models
> - **UserGroup**
>	- id pk (shared)
>	- label (shared)
>	- description(shared)
>	- public/private(shared)
>	- likes(shared)(list of userids)
> 	- comments (shared)(ref comments collection)
>	- locations(shared)(ref locations collection)
>	- events(shared)(ref events collection)
>	- membership(list of members)
>	- content (shared)(pages, discussion, etc)(ref to some content)

> - **Member**
> 	- membertype/memberlevel (moderator, admin, etc)