[Home](../../README.md) > Development Tools

# Development Tools
Throughout this course new tools and techniques will continually be added.  A complete list of these tools can be found at the bottom of this page, but specific instruction on using/installing these will be located in the section where it is used.  The tools/technologies listed in this documentation are the ones that must be used for all course work unless otherwise noted.  While other platforms, languages, tools, and techniques are available to developers today, the intent is that all coding done by both students and the instructor can be combined into a single coherent application. 

## Technology Listing

> ### Platform Stack
> * **OS** - Linux will be the target operating system for our deployed applications. However, students may develop on any OS provided the submitted code is written in a platform neutral way.
> * **Programming Language** - Code will be compiled for [Java 9](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html). 
> * **Web Server** - Apache Tomcat is automatically embedded.  No need to install this.
> * **Database** - Download and install the version of [MongoDB](https://www.mongodb.com/download-center#community) appropriate for the computer you will develop on.

> ### Development Tools
> * **IDE** - Lectures will be done using [Eclipse](http://www.eclipse.org/) on either Linux or Windows, but students may use any IDE or editor on any OS they choose provided no project metadata or platform specific code is contained in submitted work.  If using Eclipse be sure to download the latest version (Oxygen) of JavaEE bundle.
> * **Build Tool** -  Students must use [Apache Maven](https://maven.apache.org/) to ensure individual projects can be combined at the end of the semester.  This comes pre-installed in most popular IDEs. 
> * **Change Control** - [Git](https://git-scm.com/) comes pre-installed in many popular IDEs.  Lecture material will be distributed and all course work will be turned in using Git on [Bitbucket](https://bitbucket.org/).
> * **Web Requests** - For most testing in a web application any browser will do, but when testing web services it is often convenient to use a specialized tool for making requests.  In this course [Postman](https://www.getpostman.com/apps) will be used for web service requests in the lectures, but students are welcome to use any technique/tool they see fit.

> ### Frameworks and Java Tools
> * **Application Framework** - [Spring Boot](https://projects.spring.io/spring-boot/) will be used to demonstrate best practice Java enterprise application techniques.  Features include: Dependency Injection, Bean Validation, Java Persistence Architecture, etc.
> * **Testing Framework** - [jUnit](http://junit.org/junit4/) is one of a couple very popular unit testing frameworks. [Mockito](http://site.mockito.org/) will aid students in testing by allowing a mock object to be substituted for a real one.
> * **Web MVC** - Many MVC frameworks can be used with Spring, but for the purposes of this class [Spring MVC](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html) will be used.  
> * **Templating Engine** - [FreeMarker](https://freemarker.apache.org/) is on of several popular HTML templating engines for Java.  Editor [plugins](https://freemarker.apache.org/editors.html) can be installed for a number of IDEs.
> * **Logging** - Loggers work best when implemented as close to the event being recorded as possible.  When used as designed a chosen logger will typically be tightly coupled to an application.  To combat this Spring uses [Simple Logging Facade for Java a.k.a. SLF4J](https://www.slf4j.org/). By using the facade pattern SLF4J allows developers to change logging framework implementations without re-factoring their code.

> ### Deployment Tools (TBD)


## Readings

> * [Maven in 10 Minutes](https://javapapers.com/jee/maven-in-10-minutes/)
> * [Using Maven within Eclipse IDE - Tutorial](http://www.vogella.com/tutorials/EclipseMaven/article.html)
> * [POM Reference](https://maven.apache.org/pom.html)
> * [Set up an SSH key](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)

## Homework One

> Setting up the four tools listed below is crucial to completing homework one.  Specific installation instructions will vary by operating system, and student preferences.  The provided information only serves as a basic outline for a typical installations.

> * **Java 9** - Be sure to install the JDK for java 9.  Installation wizards are available for most operating systems at the Oracle [website](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html).  Linux users wanting to use Advanced Package Tool(apt) may find [these](http://tipsonubuntu.com/2016/07/31/install-oracle-java-8-9-ubuntu-16-04-linux-mint-18/) instructions useful. Verify installation by using the following command `javac -version`.
> * **Eclipse Oxygen** - Students may substitute their favorite IDE for this step.  
> 	- Download [Eclipse](http://www.eclipse.org/downloads/index.php) installer.
>	- Installation on Windows seems to work best if the install wizard defaults are used.  On Linux make a folder in the home directory (i.e. eclipse_oxygen), extract the download to the new folder, and the installer should automatically run if not double click on eclipse-inst.  **Important**.  Make sure when installing to select the JavaEE package.
>	- Set Java 9 JDK as default.  Under Window>Preferences select Installed JREs select add and choose Standard VM.  Then type or choose the installation directory of you Java 9 JDK instance.  Finally, make sure the default checkbox is selected on the first modal and select apply and close.
> * **Maven** - Most IDEs include Maven by default.  To verify installation in Eclipse select File>New>Other.  Then look for Maven in the list of wizards.  If Maven was not installed with Eclipse the likely problem is that JavaEE was not selected when installing Eclipse.  For IDEs that don't provide Maven [download](https://maven.apache.org/download.cgi) and install from Apache.
>	- Create new Maven Project.
>	- File > New > Maven Project or File > New > Other > Maven > Maven Project.
> 	- Check skip archetype selection then next.
>	- For group id use edu.unk.{canvas user name}
>	- For artifact id use homework
>	- Version is required, but can be left as its default value or 0.0.1-SNAPSHOT if blank.
>	- Make sure packaging is set to jar.
>	- Fill in the name field.  This will be the project name in Eclipse.
>	- Click finished.
>	- Modify the pom.xml file with the following fields.
>		- Include a properties tag with the following child tags:
>			- java.version should have a value of 1.9
>			- project.build.sourceEncoding should have a value of UTF-8
>			- maven.compiler.source should have a value of java.version Hint: `${}`
>			- maven.compiler.target should have a value of java.version	Hint: `${}`	
>		- Include a parent tag where the groupId is org.springframework.boot, artifactId is spring-boot-starter-parent, and the version is 1.5.9.RELEASE
>		- Include a build tag with a child plugins tag that has one child plugin tag.  The plugin tag will have two children groupId with a value of org.springframework.boot, and artifactId with a value of spring-boot-maven-plugin
>		- Include an organization tag where the name is "UNK Computer Science and Information Technology Department" and the url is "http://www.unk.edu/academics/csit/index.php"
>		- Include a developers tag with one developer.  The id should be the canvas username, name should be the student's full name, and email should be UNK email address.			
> * **Git** - Eclipse will have this pre-installed.  If not provided with the chosen IDE, Git can be [downloaded](https://git-scm.com/downloads) and installed separately.
>	- Create a [Bitbucket](https://bitbucket.org/) account.  Bitbucket was chosen for this class, because they provide private repositories at the free account level.
>	- [Create](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html) an SSH key.  
>	- [Install](https://confluence.atlassian.com/bitbucketserver/ssh-user-keys-for-personal-use-776639793.html) public SSH key in Bitbucket profile.
>	- Create a new repository. 
>		- Click on the plus sign icon located in the left hand blue menu bar of Bitbucket. Then select repository.
>		- Name the new repository csit406-spring2018-{canvas user name}-homework.
>		- Access Level should be checked (private).
>		- Include README? no
>		- Version control system Git.
>		- Click Create repository.
>		- Copy the SSH url for the new repository.  This can be found on the overview page of the new repository.
>	- Link Eclipse project to Bitbucket repository.
>		- In the Eclipse project create a new file called .gitignore.  Then copy the contents of this repository's .gitignore file into the new one and save.
>		- Right click on the new project in the project explorer pane and select Team > Share Project. 
>		- This should bring up the create repository dialog.  Choose create then set the working directory to home/git/project-name.
>		- Right click on the project name again.  Select Team > Commit.  In the Git Staging pane that opens only choose .gitignore and pom.xml files and click the plus icon.
>		- Next write a commit message.  This should be something like submitting homework one.
>		- Choose the Commit and Push button.
>		- That will open a dialog box.
>			- Paste the Bitbucket repository url into the uri field.  Don't change any other fields
>			- Click the finish button.
>		- When prompted type the SSH key passphrase.
>		- If all went correctly a confirmation dialog should appear.











