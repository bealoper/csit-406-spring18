[Home](../../README.md) > Architecture

# Architecture
Java enterprise architecture differs from typical small business and personal web applications in that each resource request may represent complex business processes.  Popular web frameworks such as Rails, CodeIgniter, and Express are overwhelmingly designed around the Web MVC pattern.  This design is great for create, read, update, and delete (CRUD) applications where the business logic is limited to validating and storing data, but can become difficult to maintain when more is needed.  They also suffer from issues with flexibility.  It's common for one business process to depend internally on anther.  Calling code from a different part of an application without replicating business logic can be awkward in simple MVC applications.

Service Oriented Architecture provides a way to separate business logic from how data is presented and how it is stored.  This allows organizations to create a single piece of business logic that can be exposed easily to client-server, web interfaces, web service APIs, and other programs all at the same time.  It also allows organizations to move between database products and platforms without having to re-factor their applications. 

> ## Readings
> * [S.O.L.I.D Object Oriented Design](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)
> * [Service Oriented Architecture](https://en.wikipedia.org/wiki/Service-oriented_architecture)
> * [Microservices](https://en.wikipedia.org/wiki/Microservices)
> * [Understanding Model-View-Controller](https://blog.codinghorror.com/understanding-model-view-controller/)
> * [Spring Framework Architecture](https://dzone.com/articles/spring-framework-architecture)
> *	[Understanding Spring Web Application Architecture: The Classic Way](https://www.petrikainulainen.net/software-development/design/understanding-spring-web-application-architecture-the-classic-way/)
> * [SOA Diagram](../pdf/SOA Diagram.pdf)

> ## Project
> For this term students will design and create a Java enterprise application.  The size of this application will be scaled to the size of the class.  Each student will be responsible for one or more business processes and all related web pages, repositories, and web services.  At the completion of this section the class must decide on a project, identify business processes, and assign those processes to individual students.  In addition, the class must setup their Bitbucket repository and ensure all students can pull/push to it.