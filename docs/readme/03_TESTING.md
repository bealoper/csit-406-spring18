[Home](../../README.md) > Testing

# Testing
Testing is an important part of software development.  Often organizations will employ dedicated quality assurance personnel to verify the code produced meets the stated requirements.  These testers are important to make sure the software that ultimately ships works correctly, but sending code back to the developer to correct defects wastes valuable time.  More recently organizations have been turning to test driven development where developers craft simple tests to prove their code meets the given requirements.  This has two main benefits.  First, it rewards the programmer for writing clean modular code.  Second, all tests become part of the code base and can be run automatically as part of the deployment process.

In this course we will concentrate primarily on unit testing, but may also write some integration tests.  To keep these test types clear, test classes should have either UnitTest or IntegrationTest appended to the class name when turning in work.

> ## Readings
> * [What is software testing? What are the different types of testing?](https://www.codeproject.com/Tips/351122/What-is-software-testing-What-are-the-different-ty)
> * [Unit Tests, How to Write Testable Code and Why it Matters](https://www.toptal.com/qa/how-to-write-testable-code-and-why-it-matters)
> * [JUnit Testcases in Java: Simple JUnit 4.12 Hello World Tutorial with All in One Details](http://crunchify.com/simple-junit-4-tutorial-hello-world-example/)
> * [Getting started with Hibernate Validator](http://hibernate.org/validator/documentation/getting-started/)
> * [Spring MVC Custom Validation](http://www.baeldung.com/spring-mvc-custom-validator)
> * [Unit tests with Mockito - Tutorial](http://www.vogella.com/tutorials/Mockito/article.html)

> ## Homework Two
> Create a user entity class with a custom annotation for passwords.  Use a jUnit test case to prove the annotation works.
> ### Requirements
> * **User**
> 	- Create an interface called User with a package name is edu.unk.csit406.user.entity.
>	- with only getter methods for the fields below in UserEntity
> * **User Entity**  
>	- Create a class called UserEntity that implements User with a package name is edu.unk.csit406.user.entity and where all fields are private and have a public setter and getter for each field.
>	- Add the following fields, and annotate each field to the stated requirements.
>		- **id:** string required.
>		- **username:** string required. 
>		- **password:** string required, minimum 8 characters, and valid for the custom annotation below.
>		- **email:** string required, valid email address. 
> *	**Custom Validation Annotation**
> 	- Create an annotation called @Password that conforms to the javax.validation specification
> 	- The message for this annotation must read "Passwords must contain at least one lower case letter, one upper case letter, one number, and one special character".
> * **Validating Class**
>	- Create a class called ValidatePassword that implements ConstraintValidator<Password, String>
>	- Passwords must contain at least one each of the following: lower case letter, upper case letter, number, and a special character.
>	- The isValid method must ensure the presence of all character types when not null, but not check the length.  That should be done using a standard javax.validation constraint in the entity class.
>	- Write a unit test called ValidPasswordUnitTest that proves the isValid method works according to the requirements 
> * **Patterns**
>	- Place all regular expressions needed for this homework in a class called ValidationPatterns.
>	- Write a unit test called ValidationPatternsUnitTest that proves each regular expression works.
> * **Documentation**
>	- At a minimum document each class and method using standard Javadoc notation.
