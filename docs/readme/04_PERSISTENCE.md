[Home](../../README.md) > Persistence

# Persistence
A best practice in developing web applications is to avoid platform specific code whenever possible.  One place this is especially important is in the persistence layer.  Keeping the footprint of database specific code small provides flexibility for the organization to change its infrastructure without rewriting large parts of its applications.  It can also be an important selling point for the customers of a software company that don't want to buy a license for a database platform they don't use.  In modern Java development this practice has become the javax.persistance specification. 

This specification is commonly known as Java Persistence Architecture or JPA for short.  JPA is powerful in that it lets Java developers concentrate on writing their code, and moves responsibility for writing SQL to the database vendors.                        

In this course we will be using Spring Data on MongoDB.  In most cases Spring Data is really just JPA, but the JPA specification has not yet expanded to include NoSQL database platforms.  So Spring has created JPA consistent mechanisms from MongoDB and one or two other platforms.  

Regardless of the platform I find it much easier to develop when I have direct access to the database.  It is recommended that students take the time to learn some basic MongoDB commands.  At a minimum students should be able to start and stop the DB server, switch databases on the server, view collections for a database, and perform some simple queries on a collection.

> ## Readings
> * [The key differences between SQL and NoSQL DBs.](http://www.monitis.com/blog/cc-in-review-the-key-differences-between-sql-and-nosql-dbs/)
> * [Mongo Shell Quick Reference](https://docs.mongodb.com/manual/reference/mongo-shell/)
> * [Don’t use DAO, use Repository ](https://thinkinginobjects.com/2012/08/26/dont-use-dao-use-repository/)
> * [Chapter 2 Section 2.2. apping with JPA (Java Persistence Annotations)](https://docs.jboss.org/hibernate/stable/annotations/reference/en/html/entity.html#entity-mapping)
> * [Spring Boot + Spring Data MongoDB example](https://www.mkyong.com/spring-boot/spring-boot-spring-data-mongodb-example/)
> * [Working with Spring Data Repositories](https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html)

> ## Homework Three
> Create a repository for the UserEntity created in homework two.  At this time we will not be encrypting the password.  In Spring this is part of the security suite, which we cover near the end of the semester. 
> ### Requirements 
> * **Main Class**
>	- Create a class called Application in src/test/java with a package name edu.unk.csit406. 
>	- This class should be annotated to match the Application Class in this project.  Make sure that the username field is indexed and unique.
>	- This class should have a main method to match the Application Class in this project.
> * **User Entity Annotations**
> 	- Add createdDate and lastModifiedDate fields to the UserEntity class.  Provide getter methods, but not setter methods.
> 	- Annotate the UserEntity class with all appropriate javax.persistance/org.springframework.data.annotation annotations.
>	- The collection name for this entity must be "users".
> * **User Repository Custom**
>	- Create an interface called UserRepositoryCustom  with a package name edu.unk.csit406.user.repository
>	- In this interface declare a method updateUserEntity which both takes UserEntity as an argument and returns a UserEntity.
>	- Create a class UserRepositoryImpl that implements the UserRepositoryCustom interface in the same package.
>	- Implement the updateUserEntity method so that only the username, password, and email fields can be modified.
> 	- The method should return the updated entity if saving was successful, and null if it is not.
> * **User Repository**
> 	- Create an interface called UserRepository with a package name edu.unk.csit406.user.repository
>	- This class must extend MongoRepository with the appropriate generic parameters, and UserRepositoryCustom.
> 	- Create a magic findBy query that retrieves a document by using the username field. 
> * **User Repository Integration Test**
> 	- Write an integration test called UserRepositoryIntegrationTest in the same package as the UserRepository interface.
>	- The class should have three test methods.
>		- Test that the UserEntity saves into the database correctly.  This includes verifying that any audit fields get populated, and that the username field unique index is enforced.
>		- Test that the magic findBy query correctly retrieves documents from the database collection.
>		- Test that the custom updateUserEntity method works.  Prove that only the username, password, and email fields can be changed.
>	- Make sure all tests remove any data they create.












