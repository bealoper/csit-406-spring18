[Home](../../README.md) > Services

# Services
The service class is where all business logic resides.  To the greatest extent possible the service class should be as loosely coupled as possible from other parts of the application.  This can be accomplished by requiring all dependencies be injected from outside the class, and by publishing a firm contract to users of the service through interfaces.  

> ## Readings
> * [Dependency injection](https://en.wikipedia.org/wiki/Dependency_injection)
> * [Is Dependency Injection Replacing the Factory Patterns?](http://tutorials.jenkov.com/dependency-injection/dependency-injection-replacing-factory-patterns.html)
> * [17. Spring Beans and dependency injection](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-spring-beans-and-dependency-injection.html)
> * [Tutorial: Correct SLF4J logging usage and how to check it](https://gualtierotesta.wordpress.com/2016/02/27/tutorial-correct-slf4j-logging-usage-and-how-to-check-it/)

> ## Homework Four
> Create a service class for the user business process.
> ### Requirements
> * **Create User DTO**
>	- Create a class called CreateUserDTO.
>	- The class should have a username, password, and email field.
>	- Add setters and getters for each field.
>	- Add appropriate validation annotations
> * **Update User DTO**
>	- Create a class called UpdateUserDTO
> 	- The class should have an id, username, password, and email field.
>	- The class should have setters and getters for each field.
>	- Add appropriate validation annotations
> * **Service Interface**
> 	- Create an interface called UserService with the following methods:
>		- create: accepts a CreateUserDTO object and returns User object.
>		- get: accepts a String id and returns a User object.
>		- find: accepts a String username and returns a User object.
>		- find: accepts no arguments and returns a list of type User.
>		- update: accepts an UpdateUserDTO object and returns a User object.
>		- delete: accepts a String id and returns a boolean.
> * **Service Implementation**
>	- Create a class called UserServiceImpl that implements UserService.
>	- This class must implement logger, and log at a minimum exceptions.
>	- Constructor must accept the UserRepository and a Validator ensuring they are not null.
>	- Method requirements
>		- create: do the following at a minimum
>			- Validate the dto.
>			- Map dto fields to the UserEntity leaving id null.
>			- Save the UserEntity. Note: The class example created an id manually to demonstrate making custom validation annotations.  When left null the database will generate an id automatically.
>		- get: return the requested User from the repository.
>		- find: the one argument find method should return the requested User from the repository using the magic find by method in homework three
>		- find: the no argument find method should return a list of all users from the UserRepository.
>		- update: do the following at a minimum
>			- Validate the dto.
>			- Map dto fields to the UserEntity making sure that it is not possible to create a new document or wipe out existing data that is not passed.
>			- Save the UserEntity.
> *	**Unit Tests**
>	- Create a unit test class for each method in the UserService method.
>		- Each class should be named as follows methodNameUnitTest
>		- Within each test class create test methods to test each possible route through the method.  This should be similar to what we did in class.
>	- Create a test suite class called UserServiceTestSuite that references all tests for the user service
>	- Make sure to verify that running the test suite class as jUnit executes all the unit test classes.





