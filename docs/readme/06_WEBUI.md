[Home](../../README.md) > Web UI

# Web UI

> ## Readings
> * [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
> * [W3.CSS Tutorial](https://www.w3schools.com/w3css/default.asp)
> * [Unicode Characters](http://xahlee.info/comp/unicode_index.html)
> * [FreeMarker](https://freemarker.apache.org/)
> * [Freemarker template inheritance](https://nickfun.github.io/posts/2014/freemarker-template-inheritance.html)

> ## Homework Five
> Create the controllers and views for the UserService created in homework four.   
> ### Requirements
> * **Controller**
>	- Create a class called UserWebController annotated with @Controller and @RequestMapping to /users.
>	- Provide a constructor for the method that accepts the UserService interface and assigns it to a private field.
>	- Create the following methods each should perform the correct calls to the service method.
>		- index
>			- Accepts Model
>			- Maps to request method GET
>			- Path is /
>			- Sets a list of all users in the model
>			- Returns the string "users/index"
>		- registerForm
>			- Accepts Model
>			- Maps to request method GET
>			- Path is /register
>			- Returns the string "users/register"
>		- registerSubmit
>			- Accepts CreateUserDTO object
>			- Maps to request method POST
>			- Path is /register
>			- Calls the UserService create method
>			- Returns a redirect string to "/users/edit/{id}
>		- editForm
>			- Accepts Model
>			- Maps to request method GET
>			- Path is /edit/{id}
>			- Returns the string "users/edit"
>		- editSubmit
>			- Accepts UpdateUserDTO object
>			- Maps to request method POST
>			- Path is /edit/{id}
>			- Calls the UserService update method
>			- Returns a redirect string to "/users/edit/{id}
>		- loginForm
>			- Accepts Model
>			- Maps to request method GET
>			- Path is /login
>			- Returns the string "users/edit/{id}"
>		- delete
>			- Maps to request method GET
>			- Path is /delete/{id}
>			- Returns a redirect string to "/users/"
> * **Views**
>	- Create a FreeMarker template for the site theme.
>		- The file should be called theme.ftl and be located in the folder templates under /src/main/resources.
>		- You may create any theme.  It's recommended to use a template from [W3.CSS Templates](https://www.w3schools.com/w3css/w3css_templates.asp).  Toward the bottom of the page there are some templates that would work well for transaction pages.
>		- All CSS, JS, and images files should be located under /src/main/resources in a folder called public.  The public folder can be structured according to the student's preferences.
>		- Title and h1 elements should get their value dynamically. Note: use the technique shown in "Freemarker template inheritance" from the readings.
>	- Create views for each of the controller methods above that don't redirect.
>		- Create a folder called users templates folder.
>		- All user views must be in this folder.
>		- The users/edit and users/index template must populate the field/table/list values from the UserService data.
>		- All form submissions must correctly populate the DTO objects.
> * **Tests**
>	- Create a test suite for the UserWebController called UserWebControllerTestSuite
>	- Create a test case for each method in the controller showing that it operates correctly.




