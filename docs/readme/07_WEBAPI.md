[Home](../../README.md) > Web API

# Web API

> ## Readings
> * [Representational state transfer](https://en.wikipedia.org/wiki/Representational_state_transfer)
> * [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
> * [47. Endpoints](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)
> * [Jackson Annotations](http://tutorials.jenkov.com/java-json/jackson-annotations.html)
> * [Latest Jackson integration improvements in Spring](https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring)

> ## Homework Six
> Create restful endpoints for the UserService.
> ### Requirements
>	- Create a class called UserRestController annotated with @RestController and @RequestMapping to /rest/users.
>	- Provide a constructor for the method that accepts the UserService interface and assigns it to a private field.
>	- Create the following methods each should perform the correct calls to the service method.
>		- index
>			- Maps to request method GET
>			- Path is /
>			- Returns a list of User objects
>		- create
>			- Accepts request body CreateUserDTO
>			- Maps to request method POST
>			- Returns the User object that was created.
>		- get
>			- Maps to request method GET
>			- Path is /{id}
>			- Returns a User object
>		- update
>			- Accepts request body UpdateUserDTO
>			- Maps to request method POST
>			- Path is /{id}
>			- Returns returns the updated User object
>		- delete
>			- Maps to request method DELETE
>			- Path is /{id}
>			- Returns the deleted User object
> * **Tests**
>	- Create a test suite for the UserRestController called UserRestControllerTestSuite
>	- Create a test case for each method in the controller showing that it operates correctly.
