[Home](../../README.md) > Deployment

# Deployment

> ## Readings
> [How to Connect to Spring BOOT Rest service to Mongo DB in Docker](http://www.littlebigextra.com/how-to-connect-to-spring-boot-rest-service-to-mongo-db-in-docker/)

> ## Activity
> * **Create App Docker Image**
> 	1. Create a file called Dockerfile in the root directory of your project with the following contents:
>		```` text
>		FROM java:8
>		VOLUME /tmp
>		ADD target/class-workspace-deploy-0.0.1-SNAPSHOT.jar /app.jar
>		EXPOSE 8080
>		ENTRYPOINT ["java","-Dspring.data.mongodb.uri=mongodb://mongo/test","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
>		````
> 	2. Open a command prompt
>		* Change directory to the location of your new Dockerfile.
>		* Run the following command. You may need to run as administrator/sudo.
>		````bash
>		$ docker build -t spring-boot-mongo:latest .
>		````
>		* Verify image was created using the following:
>		````bash
>		$ docker images
>		REPOSITORY TAG   IMAGE  ID          CREATED
>		springboot-mongo latest 3e39311752f1 5 seconds ago
>		````
> * **Create Database Docker Image** 
>	1. Run the following commands:
>		````bash
>		$  docker run -d -p 27000:27017 --name mongo mongo
>		````
>		* Verify image was created using the following:
>		````bash
>		$ docker images
>		REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
>		spring-boot-mongo   latest              feaf2acf20ae        6 minutes ago       643 MB
>		mongo               latest              a0f922b3f0a1        9 days ago          366 MB
>		````
> * **Start the application**
>	1. Run the following command
>		````bash
>		$ docker run -p 8080:8080 --name spring-boot-mongo-app --link=mongo  spring-boot-mongo
>		````
> * **TEST**
>	1. Visit localhose:8080 in your browser




