package edu.unk.csit406.branding.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/branding")
public class BrandingProjectWebController {
	
	final private static String PROJECT_ID = "bdf3b800-3c5f-4b91-abc3-87901d528ecf";//THIS WILL NEED TO BE REPLACED WITH A SERVICE
	
	@GetMapping("/")
	public String getProjects(Model model) {
		model.addAttribute("title","Demo Project");
		model.addAttribute("projectId",PROJECT_ID);
		return "branding/index";
	}
}
