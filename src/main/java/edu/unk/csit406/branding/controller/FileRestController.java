package edu.unk.csit406.branding.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Objects;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

@RestController
@RequestMapping("/rest/branding/files")
public class FileRestController {

	private final FileService service;
	
	public FileRestController(FileService service) {
		this.service = Objects.requireNonNull(service);
	}
	
	@GetMapping("/{projectId}")
	public @ResponseBody FileStructureDTO index(@PathVariable(value="projectId", required=true) String projectId) {
		FileStructureDTO dto = this.service.getFileStructure(projectId);
		
		if(dto == null) {
			throw new NotFoundException("file not found");
		}
		
		return dto;
	}
	
	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody File createFile(@RequestBody FileCreateDTO dto) {
		return this.service.create(dto);		
	}
	
	@GetMapping("/delete/{id}") 
	public @ResponseBody FileStructureDTO delete(@PathVariable(value="id", required=true) String id) {
		File deleted = this.service.delete(id);
		FileStructureDTO dto = this.service.getFileStructure(deleted.getProjectId());
		
		return dto;
	}
	
	@GetMapping("/edit/{id}")
    public @ResponseBody File getFile(@PathVariable(value="id", required=true) String id) {
		File file = this.service.get(id);
		
		if(file == null) {
			throw new NotFoundException("file not found");
		}
		
		return file;
    }
	
	@PostMapping("/edit/{id}")
    public @ResponseBody File saveFile(@PathVariable(value="id", required=true) String id, @RequestBody FileUpdateDTO dto) {
		File file = this.service.update(dto);
		
		if(file == null) {
			throw new NotFoundException("file not found");
		}
		
		return file;
    }
	
	@RequestMapping(value="/preview/{id}", method=RequestMethod.GET, produces="application/octect-stream")
	public ResponseEntity<InputStreamResource> preview(@PathVariable(value="id", required=true) String id) {
		File file = this.service.get(id);
		
		if(file == null) {
			throw new NotFoundException("file not found");
		}
		
		HttpHeaders headers = new HttpHeaders();
		int length = file.getBytes().length;
		InputStream stream = new ByteArrayInputStream(file.getBytes());
		
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
			
		
		return (ResponseEntity<InputStreamResource>) ResponseEntity
				.ok()
				.headers(headers)
				.contentLength(length)
				.contentType(MediaType.parseMediaType("application/octect-stream"))
				.body(new InputStreamResource(stream));	
	}
}
