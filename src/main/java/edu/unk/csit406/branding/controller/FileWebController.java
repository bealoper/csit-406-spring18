package edu.unk.csit406.branding.controller;

import java.util.Objects;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;
import edu.unk.csit406.validation.ValidationPatterns;

@Controller
@RequestMapping("/branding/files")
public class FileWebController {

	private final FileService service;
	
	public FileWebController(FileService service) {
		this.service = Objects.requireNonNull(service);
	}
	
	@GetMapping("/react/{projectId}")
	public String react(@PathVariable(value="projectId", required=true) String projectId, Model model) {
		model.addAttribute("projectId", projectId);
		return "branding/react/index";		
	}
	
	@GetMapping("/{projectId}")
	public String index(@PathVariable(value="projectId", required=true) String projectId, Model model) {
		FileStructureDTO dto = this.service.getFileStructure(projectId);
		
		if(dto == null) {
			throw new NotFoundException("project not found");
		}
		
		//need a test to verify valid projectId  do service and pattern tests
		
		model.addAttribute("title","Demo Project Files");
		model.addAttribute("projectId", projectId);
		model.addAttribute("fileStructure", dto);
		model.addAttribute("pathPattern", ValidationPatterns.PATH_PATTERN);
		
		return "branding/files/index";
	}	
	
	@PostMapping("/create")
	public String createFile(@ModelAttribute FileCreateDTO createDTO) {
		this.service.create(createDTO);
				
		return "redirect:/branding/files/" + createDTO.getProjectId();		
	}
	
	@GetMapping("/edit/{id}")
	public String getFile(@PathVariable(value="id", required=true) String id, Model model) {
		File file = this.service.get(id);
		
		if(file == null) {
			throw new NotFoundException("project not found");
		}
		
		model.addAttribute("title","Edit - " + file.getPath());
		model.addAttribute("file", file);
		
		return "branding/files/edit";
	}
	
	@PostMapping("/edit/{id}")
	public String saveFile(@ModelAttribute FileUpdateDTO updateDTO, Model model) {
		File file = this.service.saveData(updateDTO.getId(), updateDTO.getData());
		
		model.addAttribute("title","Edit - " + file.getPath());
		model.addAttribute("file", file);
		return "branding/files/edit";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteFile(@PathVariable(value="id", required=true) String id) {
		File file = this.service.delete(id);
		
		if(file == null) {
			throw new NotFoundException("file does not exist");
		}
		
		return "redirect:/branding/files/" + file.getProjectId();	
	}
}
