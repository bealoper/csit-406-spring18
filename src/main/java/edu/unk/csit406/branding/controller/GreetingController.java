package edu.unk.csit406.branding.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

	@RequestMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name,  Model model) {
		String greeting = "Hello " + name;
		
		model.addAttribute("message", greeting);
		
		return "greeting";
	}
	
	@PostMapping("/greeting/submit")
	public String greetingSubmit(@RequestParam(name="name", required=false, defaultValue="World") String name,  Model model) {
		String greeting = "Hello " + name;
		
		model.addAttribute("message", greeting);
		
		return "greeting";
	}
	
	@GetMapping("/demo")
	public String showDemo() {
		return "demo-layout";
	}
	
	@GetMapping("/freemarker")
	public String freemarker(Model model) {
		//if statements
		model.addAttribute("ifhasvalue", "This Value Exists");
		
		//loops 
		List<String> items = new ArrayList<String>();
		items.add("one");
		items.add("two");
		model.addAttribute("list1", items);
		
		List<String> items2 = new ArrayList<String>();
		model.addAttribute("list2", items2);
		
		return "freemarker-examples";
	}
	
	@GetMapping("/")
	public String getHome() {
		return "home";
	}
}
