package edu.unk.csit406.branding.dto;

import java.time.LocalDateTime;

import edu.unk.csit406.branding.entity.MimeType;

public interface File {
	public LocalDateTime getCreatedDate();	
	public byte[] getBytes();	
	public String getData();
	public String getId();
	public LocalDateTime getLastModifiedDate();
	public String getPath();
	public String getProjectId();
	public MimeType getType();
	public Long getVersion();
}
