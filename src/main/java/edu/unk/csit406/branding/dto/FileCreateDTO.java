package edu.unk.csit406.branding.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import edu.unk.csit406.validation.IsUUID;
import edu.unk.csit406.validation.ValidationPatterns;

public class FileCreateDTO {
	
	@NotNull
	@Pattern(regexp = ValidationPatterns.PATH_PATTERN)
	private String path;
	
	@NotNull
	@IsUUID
	private String projectId;
	
	public String getPath() {
		return this.path;
	}
	
	public String getProjectId() {
		return this.projectId;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}
