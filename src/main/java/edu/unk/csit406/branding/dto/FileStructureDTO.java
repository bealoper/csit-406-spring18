package edu.unk.csit406.branding.dto;

import java.util.List;

public interface FileStructureDTO {
	
	public List<FileStructureDTO> getChildren();
	
	public String getId();
	
	public String getName();
	
	public String getPath();
	
	public boolean isLeaf();
	
	public boolean isRoot();
}
