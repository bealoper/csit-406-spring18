package edu.unk.csit406.branding.dto;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FileStructureDTOImpl implements FileStructureDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(FileStructureDTOImpl.class);	
	
	private List<FileStructureDTO> children = new LinkedList<FileStructureDTO>();
	
	private String id;
	
	private String path;
	
	public FileStructureDTOImpl(String id, String path) {
		this.id = id;
		this.path = Objects.requireNonNull(path);
	}
		
	public void addChild(FileStructureDTO child) {
		this.children.add(child);
	}


	@Override
	public List<FileStructureDTO> getChildren() {
		return this.children;
	}


	@Override
	public String getId() {
		return this.id;
	}


	@Override
	public String getName() {			
		if(this.isRoot()) {
			return "root";
		} else {
			String[] parts = this.path.split("/");
			return parts[parts.length - 1];
		}
	}


	@Override
	public String getPath() {
		return this.path;
	}

	@Override
	public boolean isLeaf() {
		return this.id != null;
	}

	@Override
	public boolean isRoot() {
		return this.path == "/";
	}	
	
	public void logTree(int level) {
		if(logger.isDebugEnabled()) {//don't do all this work if logging level is too low
			String tabs = "";
			
			//create tab stops
			for(int i = 0; i < level; i++) {
				tabs += "\t";
			}
			
			//log this
			if(this.isLeaf()) {
				logger.debug("{}Leaf({})", tabs, this.getName());
			} else {
				logger.debug("{}Branch({}) Size:{}", tabs, this.getName(), this.getChildren().size());
				
				//log children
				for(FileStructureDTO child : this.getChildren()) {
					FileStructureDTOImpl childImpl = (FileStructureDTOImpl) child;
					childImpl.logTree(level + 1);
				}
			}
		}
	}
}
