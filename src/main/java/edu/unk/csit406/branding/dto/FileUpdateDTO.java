package edu.unk.csit406.branding.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import edu.unk.csit406.branding.entity.MimeType;
import edu.unk.csit406.validation.IsUUID;
import edu.unk.csit406.validation.ValidationPatterns;

public class FileUpdateDTO {
	
	private String data;

	@IsUUID
	@NotNull
	private String id;
	
	@Pattern(regexp=ValidationPatterns.PATH_PATTERN)
	private String path;
	
	//@NotNull
	private MimeType type;
	
	
	public String getData() {
		return this.data;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public MimeType getType() {
		return this.type;
	}
	
	public void setData(String data) {
		this.data = Objects.requireNonNull(data);
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void setType(MimeType type) {
		this.type = type;
	}
}
