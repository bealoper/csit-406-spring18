package edu.unk.csit406.branding.entity;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.validation.IsUUID;
import edu.unk.csit406.validation.ValidationPatterns;

/**
 * Represents a file that can be edited by a user.
 * In this model we are setting the id manually (demo of validation) so Spring auditor cannot determine if this
 * is a new or existing object.  To overcome this we are using @Version which plays well into the purpose of this
 * domain.  It works, because we leave version null.  Another catch is the version field must be data type Long. 
 * Notice the capital L.  
 * @author pswenson
 *
 */
@Document(collection="branding_files")
public class FileEntity implements File {

	@CreatedDate
	private LocalDateTime createdDate;
	
	private byte[] data = new byte[0];
	
	@Id
	@NotNull
	@IsUUID
	private String id;	
	
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
	
	@NotNull
	@Pattern(regexp = ValidationPatterns.PATH_PATTERN)
	private String path;
	
	@Indexed
	@NotNull
	@IsUUID
	private String projectId;
	
	@NotNull
	private MimeType type;
	
	@Version
	private Long version;
		
	public byte[] getBytes() {
		return this.data;
	}
	
	public LocalDateTime getCreatedDate() {
		return this.createdDate;
	}
	
	public String getData() {
		return new String(this.data);
	}
	
	public String getId() {
		return this.id;
	}
	
	public LocalDateTime getLastModifiedDate() {
		return this.lastModifiedDate;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public String getProjectId() {
		return this.projectId;
	}
	
	public MimeType getType() {
		return this.type;
	}
	
	public Long getVersion() {
		return this.version;
	}
	
	public void setBytes(byte[] data) {
		this.data = data;
	}
	
	public void setData(String data) {
		this.data = data.getBytes();
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public void setType(MimeType mimeType) {
		this.type = mimeType;
	}
}
