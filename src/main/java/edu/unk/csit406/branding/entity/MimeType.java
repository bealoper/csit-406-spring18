package edu.unk.csit406.branding.entity;

public enum MimeType {
	CSS("text/css", "Cascading Stylesheets"),
	FTL("text/html", "Free Marker Template"),
	GITIGNORE("text/plain", "Gitignore File"),
	HTML("text/html", "Hypertext Markup Language"),	
	JS("text/javascript","JavaScript"),
	JSON("application/json", "JavaScript Object Notation"),
	LESS("text/css", "Less StyleSheet"),
	PROPERTIES("text/plain", "Properties File"),
	SASS("text/css", "Sass StyleSheet"),
	TEXT("text/plain", "Plain Text"),
	XML("text/xml", "Extensible Markup Language");
	
	private String contentType;
	private String label;
	
	private MimeType(String contentType, String label) {
		this.contentType = contentType;
		this.label = label;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public String getLabel() {
		return this.label;
	}
}
