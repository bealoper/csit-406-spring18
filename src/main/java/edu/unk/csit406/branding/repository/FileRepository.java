package edu.unk.csit406.branding.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.unk.csit406.branding.entity.FileEntity;

@Repository
public interface FileRepository extends MongoRepository<FileEntity, String>, FileRepositoryCustom {
	
	public List<FileEntity> findByProjectIdOrderByPathAsc(String projectId);
}
