package edu.unk.csit406.branding.repository;

import edu.unk.csit406.branding.entity.FileEntity;

public interface FileRepositoryCustom {
	public FileEntity updateFileEntity(FileEntity entity);
}
