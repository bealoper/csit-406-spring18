package edu.unk.csit406.branding.repository;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.WriteResult;

import edu.unk.csit406.branding.entity.FileEntity;

public class FileRepositoryImpl implements FileRepositoryCustom {

	@Autowired
    MongoTemplate mongoTemplate;

	@Override
	public FileEntity updateFileEntity(FileEntity entity) {
		Query query = new Query(Criteria.where("id").is(entity.getId()));
		Update update = new Update();
        update.set("path", entity.getPath());
        update.set("data", entity.getBytes());
        update.set("lastModifiedDate", LocalDateTime.now());
                
        WriteResult result = mongoTemplate.updateFirst(query, update, FileEntity.class);
		
        //http://api.mongodb.com/java/current/com/mongodb/WriteResult.html
        if(result!=null) {
        	return entity;
        } else {
        	return null;
        }		
	}
	

}
