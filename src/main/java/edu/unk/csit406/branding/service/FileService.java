package edu.unk.csit406.branding.service;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.dto.FileUpdateDTO;

public interface FileService {
	public File create(FileCreateDTO dto);
	public File delete(String id);
	public File get(String id);
	public FileStructureDTO getFileStructure(String projectId);
	public File rename(String id, String name);
	File saveData(String id, String data);
	File update(FileUpdateDTO dto);
}
