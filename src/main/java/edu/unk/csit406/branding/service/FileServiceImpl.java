package edu.unk.csit406.branding.service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.dto.FileStructureDTOImpl;
import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.entity.MimeType;
import edu.unk.csit406.branding.repository.FileRepository;

@Service
public class FileServiceImpl implements FileService {
	
	private final static Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);
	private final FileRepository repository;
	private final Validator validator;
	
	public FileServiceImpl(FileRepository repository, Validator validator) {
		this.repository = Objects.requireNonNull(repository);
		this.validator = Objects.requireNonNull(validator);
	}

	@Override
	public File create(FileCreateDTO dto) {
		dto = Objects.requireNonNull(dto);
		
		Set<ConstraintViolation<FileCreateDTO>> violations = this.validator.validate(dto);
		if(!violations.isEmpty()) {
			logger.error("Constraint Violation {}", dto.getClass().getName());
			throw new ConstraintViolationException(violations);
		}
		
		String id = UUID.randomUUID().toString();
		while(this.repository.exists(id)) {
			id = UUID.randomUUID().toString();
		}
		
		FileEntity entity = new FileEntity();
		entity.setId(id);
		entity.setPath(dto.getPath());
		entity.setProjectId(dto.getProjectId());
		
		try {
			String[] arr = dto.getPath().split("\\.");
			String ext = arr[arr.length-1].toUpperCase();
			entity.setType(MimeType.valueOf(ext));
		} catch (Exception e) {
			logger.warn("Failed to get Mime Type falling back to text", e);
			entity.setType(MimeType.TEXT);
		}
		
		return this.repository.save(entity);
	}

	@Override
	public File delete(String id) {
		File file = this.repository.findOne(id);
		
		if(file == null) {
			logger.warn("Unable to delete {} id {} not found", FileEntity.class.getName(), id);
			return null;
		} else {
			try {
				this.repository.delete(id);
				logger.info("deleted {} id {}", FileEntity.class.getName(), id);
				return file;
			} catch(Exception e) {
				logger.error("Unable to delete {} id {}", FileEntity.class.getName(), id, e);
				return null;
			}
		}
	}

	@Override
	public File get(String id) {
		logger.info("getting id {}", id);
		return this.repository.findOne(id);
	}

	@Override
	public FileStructureDTO getFileStructure(String projectId) {
		List<FileEntity> entities = this.repository.findByProjectIdOrderByPathAsc(Objects.requireNonNull(projectId));
		String rootPath = "/";
		FileStructureDTOImpl tree = new FileStructureDTOImpl(null, rootPath);
		FileStructureDTOImpl currentBranch, lastBranch;
		HashMap<String, FileStructureDTOImpl> index = new HashMap<String, FileStructureDTOImpl>();
		
		logger.info("getting file structure, project id {}", projectId);
		
		index.put(rootPath, tree);
		for(FileEntity entity : entities) {
			String[] parts = entity.getPath().split(rootPath);
			String currentPath = rootPath;
			
			currentBranch = index.get(currentPath);
			for(int i = 1; i < parts.length; i++) {
				if(i + 1 == parts.length) {//leaf
					currentBranch.addChild(new FileStructureDTOImpl(entity.getId(),entity.getPath()));
				} else {//branch
					lastBranch = currentBranch;
					currentPath += parts[i] + rootPath;
					currentBranch = index.get(currentPath);
					
					if(currentBranch == null) {
						currentBranch = new FileStructureDTOImpl(null,currentPath);
						lastBranch.addChild(currentBranch);
						index.put(currentPath, currentBranch);						
					}
				}
			}
			
		}
				
		tree.logTree(0);
		return tree;
		/*List<FileEntity> entities = this.repository.findByProjectIdOrderByPathAsc(Objects.requireNonNull(projectId));
		File[] entityArr = (File[]) entities.toArray();
		return this.loader.load(Arrays.asList(entityArr));*/
	}

	@Override
	public File rename(String id, String path) {
		FileUpdateDTO dto = new FileUpdateDTO();
		dto.setId(Objects.requireNonNull(id));
		dto.setPath(Objects.requireNonNull(path));
		return this.update(dto);
	}
	
	@Override
	public File saveData(String id, String data) {
		FileUpdateDTO dto = new FileUpdateDTO();
		dto.setId(Objects.requireNonNull(id));
		dto.setData(Objects.requireNonNull(data));
		return this.update(dto);
	}

	@Override
	public File update(FileUpdateDTO dto) {
		
		//validate arguments
		dto = Objects.requireNonNull(dto,"argument zero must not be null");
		Set<ConstraintViolation<FileUpdateDTO>> violations = this.validator.validate(dto);
		if(violations.size() > 0) {
			ConstraintViolationException e = new ConstraintViolationException("dto is invalid", violations);
			logger.error("Failed to validate {}", FileCreateDTO.class.getName(), e);
			throw e;
		}
		
		//find entity
		FileEntity entity;
		try {
			entity = Objects.requireNonNull(this.repository.findOne(dto.getId()));
		} catch(NullPointerException e) {
			logger.error("unable to update {} id {} does not exist", FileEntity.class.getName(), dto.getId(), e);
			return null;
		}
		
		//set provided fields
		if(dto.getPath() != null) {
			entity.setPath(dto.getPath());
		}
		
		if(dto.getData() != null) {
			entity.setData(dto.getData());
		}
		
		if(dto.getType() != null) {
			entity.setType(dto.getType());
		}
		
		//Save Changes
		entity = this.repository.save(entity);
		
		logger.info("Updated {} id {} and path {}", FileEntity.class.getName(), entity.getId());
		
		return entity;
		
	}
}
