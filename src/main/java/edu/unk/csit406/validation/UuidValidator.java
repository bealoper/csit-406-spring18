package edu.unk.csit406.validation;

import java.util.UUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UuidValidator implements ConstraintValidator<IsUUID, String>{

	public void initialize(IsUUID arg0) {
		// TODO Auto-generated method stub
		
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		if(value == null) {
			return true;
		}
		
		try {
			UUID.fromString(value);
			return true;
		} catch(Exception e) {
			return false;
		}
	}

}
