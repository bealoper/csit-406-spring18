/* global Request */
/* global Headers */
/* global fetch */

/* Registered service operations */
const operations = {};

let baseUrl = null;

class Operation {
    constructor(dataIn) {
        this.endpoint = dataIn.endpoint;
        this.expires = dataIn.expires || 0;
        this.method =  (dataIn.method || "GET").toUpperCase();
        this.waitOnBusy = (dataIn.waitOnBusy === true) ? true : false;
    }
    
    /**
     * Use this method to replace endpoint wildcards
     */
    getEndpointUrl(data) {
        let url = this.endpoint;
        
        if(/\{\{.+\}\}/.test(url)) { 
            for(let n in data) {
                url = url.replace("{{" + n + "}}", encodeURIComponent(data[n]));
            }
        } 
      console.log((baseUrl !== null) ? baseUrl + url : url);
        return (baseUrl !== null) ? baseUrl + url : url;
    }
}

/* Static commands (functions) that can be called from the main page
 *    To call just pass the function name as the command parameter of doWork
 */
const commands = {
    /* Initializes a new web service operation */
    initialize: function(id, dataIn, onsuccess, onerror) {
        if (typeof dataIn === "object") {
            if (dataIn.operation && dataIn.endpoint) {
                operations[dataIn.operation] = new Operation(dataIn);
                onsuccess({},true);
            }
        }
        onerror("unable to initialize service");
    },
    /* Set a base url to use for web service requests */
    setBaseURL:function(id, url, onsuccess, onerror) {
        if(url) {
           baseUrl = url;
           onsuccess({},true);
        }
        onerror("unable to set baseUrl");
    }
};

/**
 * Temporary storage of previous requests. This object reduces unnecessary calls
 * to the server to retrieve the same data.  
 */
const cache = new class {
    constructor() {
        this.cache = {};
    }
    
    expireItem(endpoint) {
        delete this.cache[endpoint];
    }
    
    getItem(endpoint){
        return this.cache[endpoint];
    }
    
    /**
     * Sets a response to cache
     *  argument0 - The url key
     *  argument1 - The data to be stored
     *  argument2 - How long to keep the item in cache -1=forever, 0=nocache, >0=miliseconds
     */
    setItem(endpoint, response, lifespan = -1) {
        if(lifespan === 0) return;
        
        this.cache[endpoint] = response;
        if(lifespan > 0) {
            let o = this;
            setTimeout(function() {
                o.expireItem(endpoint);
            }, lifespan);
        }
    }
};

/**
 * Requests that have been dispatched to the server,
 * but have not yet yielded a response.  This object
 * aids in preventing a user from repeatedly sending
 * the same request.
 */
const inflight = new class {
    constructor() {
        this.inflight = {};
    }   
    
    getPending(endpoint) {
        return this.inflight[endpoint];
    }
    
    isPending(endpoint) { 
        return (this.inflight[endpoint]) ? true : false;
    }
    
    setPending(endpoint) {
        this.inflight[endpoint] = [];
    }
    
    unsetPending(endpoint) {
        if(this.inflight[endpoint].length === 0) {
            delete this.inflight[endpoint];
        } else {
            console.log("How do I fix this");
        }
    }
    
    push(endpoint, args, fn) {
        this.inflight[endpoint].push(args);
    }
};

class Ajax {
    constructor(id, data = {}, operation = null, inflight, cache) {
        this.id = id;
        this.data = data;
        this.operation = operation;
        this.inflight = inflight;
        this.cache = cache;
        
        if(!this.operation) throw Error("Operation is null");
    }
    
    /**
     * Makes the actual http request
     *  argument0 - options supplied by the client
     *  argument1 - what to do when complete
     */
    _request(url, success, failure) {
        const request = {
            method: this.operation.method,
            redirect: "follow",
            headers: new Headers({
                "x-request-id": this.id,
                "x-requested-with": "XMLHttpRequest",
                "Accept":"application/json",
                "Access-Control-Allow-Origin":"*",
                "Content-Type":"application/json"
            }),
            
        };
        
        if(this.operation.method === "POST") {
            request.body = JSON.stringify(this.data);
        }
        
        fetch(new Request(url, request)).then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        }).then(function(json) {
            success(json);
        }).catch(function(err) {
            failure(err);
        });
    }
    
    
    
    /**
     * The public facing interface for the server request.  This method
     * concentrates on the prescribed business process and leaves the 
     * actual mechanics of the server request to _request().
     */
    request(success, failure) {
        const o = this, 
              url = o.operation.getEndpointUrl(o.data);
        
        if(o.inflight.isPending(url)) {
            if(o.operation.waitOnBusy) {
                
            }
            //test if cue is allowed (operation property) then stash if so.
            
            //else throw errror
            throw new Error("This request is already pending");
        }
        
        //expire cache on post
        if(o.operation.method === "POST") {
            o.cache.expireItem(url);
        }
        
        //retrieve from cache instead of from the server when possible
        const cached = o.cache.getItem(url);
        if(cached) {
           success(cached, false);
        } else {
            
            //set request as in flight so we don't get multiples
            o.inflight.setPending(url, o.data);
            
            //make the request
            o._request(
                url,
                function(response) {
                    o.cache.setItem(url, response, o.operation.expires);
                    o.inflight.unsetPending(url);
                    success(response, true);
                },
                failure);
        }
    }
}



/*	This code peforms receives requests from the main window and dispatches requests as needed
 *
 */
addEventListener('message', function(e) {
    const cmd = e.data.cmd,
          dataIn = e.data.data || {},
          id = e.data.id;
          
    function onerror(err) {
        err = (typeof err === "object") ? err : {err:""};
        postMessage({
            id: id,
            data: {error:err}
        });
    }
    
    function onsuccess(dataOut, changed) {
        postMessage({
            id: id,
            data: dataOut,
            isChanged: changed
        });
    }
   
    if(!cmd) {
        onerror("Missing command");
    } else if (commands[cmd]) {//run specialized code
        try {
            commands[cmd](id, dataIn, onsuccess, onerror);
        } catch(err) {
            onerror(err);
        }
    } else {//run generic operation call
    
        const operation = operations[cmd];
        
        try {
            const ajax = new Ajax(id, dataIn, operation, inflight, cache);
            
            ajax.request(onsuccess, onerror);
        } catch(err) {
            console.log(`${cmd} threw the following error: ${err}`);
            onerror(err);
        }
    }
}, false); 