<#include "../../layout.ftl">

<#macro inject_head>
	<script src="/js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/ace/theme-twilight.js" type="text/javascript" charset="utf-8"></script>
    <script>
        ((w,d) => {
            w.addEventListener("DOMContentLoaded",() => {
                let editor = w.ace.edit("editor"),
                	isChanged = false;
                
                
                editor.setTheme("ace/theme/twilight");
                
                editor.getSession().on("change", () => {
                	isChanged = true;
                });
                
                
				let form = d.getElementById("form");
				if(form) {
					let data = form.elements["data"];
					form.addEventListener("submit",(e) => {
						data.value = editor.getSession().getValue();
					}, false);
	            }  
	            
	            let commands = d.getElementById("commands");  
	            if(commands) {
	            	commands.addEventListener("click", (e) => {
	            		if(e.target.nodeName === "A" && isChanged) {
	            			if(!w.confirm("Changes will be lost.  Are you sure?")) {
	            				e.preventDefault();
	            			} 
	            		}
	            	}, false);
	            }
            }, false);
        })(window, document);
    </script>
</#macro>

<#macro inject_body>
	<section class="theme-full-height">
		<form id="form" action="/branding/files/edit/${file.getId()}" method="POST" class="theme-full-height">
			<input type="hidden" name="data"/>
			<input type="hidden" name="type" value="${file.getType()}"/>
			<div class="w3-cell-row theme-full-height">
	            <div id="commands" class="w3-container w3-cell branding-commands theme-full-height">
	            	<a href="/branding/files/${file.getProjectId()}" class="w3-jumbo" title="Back to Explorer">&#128448;</a>
	            	<button type="submit" class="w3-jumbo" title="Save">&#128427;</button>	            	
	            </div>
	            <div class="w3-container w3-cell branding-editor theme-full-height">
	                <div id="editor">${file.getData()}</div>
	            </div>
	        </div>	
		</form>
	</section>
</#macro>

<@template/>

