<#include "../../layout.ftl">

<#macro filesandfolders tree level>   
	<#if tree.isRoot()>
  		<div class="w3-large">&#128447;<span class="node active w3-medium" data-path="${tree.getPath()}">/</span></div>
  	</#if>
	<#foreach node in tree.getChildren()>
		<div class="w3-large">
			<#list 0..<level+1 as i>&#160;&#160;&#160;</#list>
	   		<#if node.isLeaf()>
		   		&#128196;<a href="/branding/files/edit/${node.getId()}" class="node  w3-medium">${node.getName()}</a>
		   		<a href="/branding/files/delete/${node.getId()}" title="delete" class="w3-text-red">&times</a>	   	
		   	<#else> 
		   		&#128447;<span class="node  w3-medium" data-path="${node.getPath()}">${node.getName()}</span>	
		   		<@filesandfolders tree=node level=(level+1)/>
		   	</#if>
		    	   
		</div>
	</#foreach>    
</#macro>

<#macro inject_head>
	<script>
		((w,d) => {
			w.addEventListener("DOMContentLoaded", () => {
				const files = d.getElementById("files"),
					  path = d.getElementById("path");
					  
				let activePathValue = "${fileStructure.getPath()}";
				
				if(files && path) {
					path.addEventListener("focus",(e) => {
						let pathArr = path.value.split("/"),
							fileName = (pathArr.length-1 > -1 ) ? pathArr[pathArr.length-1] : "";
												
						path.value = activePathValue + fileName;
					}, false);
					
					
					let folders = Array.prototype.slice.call(files.querySelectorAll("span.node[data-path]"));
					console.log(folders);										
					files.addEventListener("click", (e) => {
						let picked = folders.filter((item) => {
							return item === e.target;
						})[0];
					
						if(picked) {
							folders.forEach((item) => {
								item.classList.remove("active");
							});
							
							activePathValue = picked.getAttribute("data-path");							
							picked.classList.add("active");							
						} else if(e.target.nodeName === "A" && e.target.href.indexOf("/delete/") > -1) {
							if(!w.confirm("Permenantly Delete File?")) {
								e.preventDefault();
							}
						}						
					}, false);
				}					
			}, false);
		})(window,document);
	</script>
</#macro>

<#macro inject_body>
	<section id="files" class="w3-container">
		<div id="picker" class="w3-margin">
    		<form action="/branding/files/create" method="POST" id="form">
    			
    			<div id="files">
    				<@filesandfolders tree=fileStructure level=0/>
    			</div>
    			
    			<div class="w3-margin-top">
	    			<input type="hidden" name="projectId" value="${projectId}"/>
	    			<input id="path" name="path" pattern="${pathPattern}" title="must be valid absolute path" required/>
	    			<input type="submit" value="Create"/>
	    		</div>
    		</form>
    		
    	</div>
    </section>
</#macro>	    	

<@template/>