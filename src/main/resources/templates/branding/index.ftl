<#include "../layout.ftl">

<#macro inject_body>
	<div class="w3-container w3-margin-top">
		<a href="/branding/files/${projectId}">Demo Basic Web UI</a>
	</div>
	<div class="w3-container w3-margin-top">
		<a href="/branding/files/react/${projectId}">Demo Restful UI</a>
	</div>
</#macro>

<@template/>