<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>React Branding App</title>
		<link rel="stylesheet" href="/css/w3.css"/>
		<link rel="stylesheet" href="/css/react-theme.css"/>
		<script src="/js/html-component-file-system-1.0.0.js"></script>
		<script src="/js/ace/ace.js" charset="utf-8"></script>
	</head>
	<body>
		<header>
			<div class="w3-top">
				<div class="w3-bar w3-black w3-card">
					<a href="/branding/" class="w3-bar-item w3-button w3-padding-large">HOME</a>
					<h1 class="w3-bar-item w3-padding-large w3-medium" id="page-title">React Branding App</h1>
				</div>
			</div>
		</header>
		<main id="app" data-rest-url="http://localhost:8080" data-project-id="${projectId}"></main>
		<script type="text/javascript" src="/react/main.98536cad.js"></script>
	</body>
</html>