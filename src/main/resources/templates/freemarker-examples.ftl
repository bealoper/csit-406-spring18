<!DOCTYPE html>
<html lang="en">
	<head>
		<title>FreeMarker Examples</title>
		<link rel="stylesheet" type="text/css" href="/css/w3.css"/>
	</head>
	<body class="w3-center">
		<header>
			<h1>FreeMarker Examples</h1>
		</header>
		<main class="w3-container">
			<h2>If Statements</h2>
			
			<#if ifhasvalue??>
				<div>It's True</div>
			<#else>
				<div>It's False</div>
			</#if>
			
			<#if ifnothasvalue??>
				<div>It's True</div>
			<#else>
				<div>It's False</div>
			</#if>
			
			<ul>
			<#list list1 as item>
				<li>${item}</li>
			</#list>
			</ul>
			
			
		<!-- userRole -->	<!-- item.role -->
			<ul>
			<#list list2 as item>
				<!--<#if userRol >= item.role>-->
					<li>${item}</li>
				<!--</#if>-->
			</#list>
			</ul>
			
			
			
			
			<#list list1>
				<ul>
					<#items as item>
						<li>${item}</li>
					</#items>
				</ul>
			<#else>
				<div class="w3-panel w3-blue">
				  <p>No Items Found in the List</p>
				</div> 
			</#list>
			
			
		</main>
</html>
