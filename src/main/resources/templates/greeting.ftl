<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Greeting</title>
	</head>
	<body>
		<h1>Greeting</h1>
		<main>
			${message}
			
			<form action="/greeting/submit" method="POST">
				<label for="yourname">What is your name</label>
				<input type="text" id="yourname" name="name"/>
				<input type="submit" value="Submit"/>
			</form>
		</main>
	</body>
</html>