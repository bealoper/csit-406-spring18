<#macro inject_head>
</#macro>

<#macro inject_body>
</#macro>

<#macro template>
<!DOCTYPE html>
	<html lang="en">
		<head>
			<title>
				<#if title??>
					${title}
				<#else>
					Application
				</#if>
			</title>	    
		    <meta charset="UTF-8">
		    <link href="/css/w3.css" rel="stylesheet" type="text/css"/>
		    <link href="/css/theme.css" rel="stylesheet" type="text/css"/>    
		    <@inject_head/>
		</head>
		<body>
		    <header>
		        <div class="w3-top">
		            <div class="w3-bar w3-black w3-card">
		                <a href="/branding/" class="w3-bar-item w3-button w3-padding-large">HOME</a>
		                <h1 class="w3-bar-item w3-padding-large w3-medium">
		                	<#if title??>
								${title}
							<#else>
								Application
							</#if>
		                </h1>
		                <!-- instance of org.springframework.web.servlet.support.RequestContext -->
		                <#if !springMacroRequestContext.requestUri?contains("/login")>
			                <form action="/logout" method="POST">
			                	<input type="hidden"  name="${_csrf.parameterName}" value="${_csrf.token}"/>
			                	<button type="submit" class="w3-bar-item w3-button w3-padding-large w3-right">LOGOUT</button>
			                </form>
			            </#if>
		            </div>
		        </div>
		    </header>
		    <main>
		     	<@inject_body/>
		    </main>	    
		</body>
	</html>
</#macro>