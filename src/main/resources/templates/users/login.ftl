<#include "../layout.ftl">
<#assign title="Login">

<#macro inject_body>
	<section class="theme-full-height w3-container w3-margin-top" style="width:500px">		
		<#if RequestParameters.error??>
			<div class="w3-panel w3-red">
		        <p>Invalid username and password.</p>
		    </div>
	    </#if>
	    <form action="/login" method="post">
	    	<input type="hidden"  name="${_csrf.parameterName}" value="${_csrf.token}"/>
	        <div>	        	
	        	<input id="username" type="text" name="username" class="w3-input"/> 
	        	<label for="username">User Name </label>
	        </div>
	        <div>	        	
	        	<input id="password" type="password" name="password" class="w3-input"/> 
	        	<label for="password"> Password </label>
	        </div>
	        <div class="w3-margin-top">
	        	<input type="submit" value="Sign In" class="w3-button w3-green"/>
	        </div>
	    </form>
	</section>	   
</#macro>

<@template/>