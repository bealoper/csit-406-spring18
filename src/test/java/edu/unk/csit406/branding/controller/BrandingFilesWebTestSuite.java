package edu.unk.csit406.branding.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	WebCreateUnitTest.class,
	WebDeleteFileUnitTest.class,
	WebGetFileUnitTest.class,
	WebIndexUnitTest.class,
	WebSaveFileUnitTest.class
})
public class BrandingFilesWebTestSuite {

}
