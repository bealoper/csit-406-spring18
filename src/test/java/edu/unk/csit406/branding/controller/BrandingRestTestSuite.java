package edu.unk.csit406.branding.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	RestIndexUnitTest.class,
	RestCreateFileUnitTest.class,
	RestDeleteUnitTest.class,
	RestGetFileUnitTest.class,
	RestPreviewUnitTest.class,
	RestSaveFileUnitTest.class
})
public class BrandingRestTestSuite {

}
