package edu.unk.csit406.branding.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.service.FileService;

public class RestCreateFileUnitTest {
	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}
	
	@Test
	public void testCreateFile() {
		FileCreateDTO dto = Mockito.mock(FileCreateDTO.class);
		File file = Mockito.mock(File.class);
		
		Mockito.when(this.service.create(dto)).thenReturn(file);
		
		File response = this.controller.createFile(dto);
		
		Assert.assertSame(file, response);
		Mockito.verify(this.service).create(dto);
	}
}
