package edu.unk.csit406.branding.controller;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.service.FileService;

public class RestDeleteUnitTest {
	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}
	
	@Test
	public void testDelete() {
		String id = UUID.randomUUID().toString();
		String projectId = UUID.randomUUID().toString();
		File file = Mockito.mock(File.class);
		FileStructureDTO dto = Mockito.mock(FileStructureDTO.class);
		
		Mockito.when(this.service.delete(id)).thenReturn(file);
		Mockito.when(this.service.getFileStructure(projectId)).thenReturn(dto);
		Mockito.when(file.getProjectId()).thenReturn(projectId);
		
		FileStructureDTO response = this.controller.delete(id);
		Assert.assertSame(dto, response);
		Mockito.verify(this.service).delete(id);
		Mockito.verify(file).getProjectId();
	}
}
