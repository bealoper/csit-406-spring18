package edu.unk.csit406.branding.controller;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class RestGetFileUnitTest {
	
	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}

	@Test
	public void testNotFound() throws Exception {
		String id = UUID.randomUUID().toString();
		Mockito.when(this.service.get(id)).thenReturn(null);
		
		try {
			this.controller.getFile(id);
			Assert.fail("Get file must throw exception when service returns null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
			Mockito.verify(this.service).get(id);
		}
	}
	
	@Test
	public void testFound() {
		String id = UUID.randomUUID().toString();
		File file = new FileEntity();
		
		Mockito.when(this.service.get(id)).thenReturn(file);
		
		try {
			File result = this.controller.getFile(id);
			Assert.assertSame(file, result);
		} catch(Exception e) {
			Assert.fail("Get file must not throw exception if service returns a file object");
		}
	}
}
