package edu.unk.csit406.branding.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class RestIndexUnitTest {

	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}
	
	@Test
	public void testNotFound() {
		String projectId = UUID.randomUUID().toString();
		Mockito.when(this.service.getFileStructure(projectId)).thenReturn(null);
		
		try {
			this.controller.index(projectId);
			Assert.fail("Index must throw exception when service returns null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
			Mockito.verify(this.service).getFileStructure(projectId);
		}
	}
	
	@Test
	public void testFound() throws URISyntaxException, IOException {
		String projectId = UUID.randomUUID().toString();
		FileStructureDTO dto = Mockito.mock(FileStructureDTO.class);
		
		Mockito.when(this.service.getFileStructure(projectId)).thenReturn(dto);
		
		try {
			FileStructureDTO response = this.controller.index(projectId);
			Assert.assertSame(dto, response);
		} catch(Exception e) {
			Assert.fail("Index must not throw exception if service returns a file object");
		}
	}
}
