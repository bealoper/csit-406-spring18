package edu.unk.csit406.branding.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class RestPreviewUnitTest {

	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}
	
	@Test
	public void testNotFound() {
		String id = UUID.randomUUID().toString();
		Mockito.when(this.service.get(id)).thenReturn(null);
		
		try {
			this.controller.getFile(id);
			Assert.fail("Get file must throw exception when service returns null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
			Mockito.verify(this.service).get(id);
		}
	}
	
	@Test
	public void testFound() throws URISyntaxException, IOException {
		String id = UUID.randomUUID().toString();
		Path path = Paths.get(ClassLoader.getSystemResource("spring_logo.png").toURI());
		byte [] bytes = Files.readAllBytes(path);
		FileEntity file = new FileEntity();
		
		file.setId(id);
		file.setBytes(bytes);
		
		Mockito.when(this.service.get(id)).thenReturn((File)file);
		
		try {
			ResponseEntity<InputStreamResource> response = this.controller.preview(id);

			Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
			Assert.assertArrayEquals(bytes, this.extractBytes(response.getBody().getInputStream()));
		} catch(Exception e) {
			Assert.fail("Get file must not throw exception if service returns a file object");
		}
	}
	
	private byte[] extractBytes(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int reads = is.read();
       
        while(reads != -1){
            baos.write(reads);
            reads = is.read();
        }
      
        return baos.toByteArray();
	}
}
