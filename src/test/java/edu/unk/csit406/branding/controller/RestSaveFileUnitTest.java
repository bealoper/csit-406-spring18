package edu.unk.csit406.branding.controller;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class RestSaveFileUnitTest {
	private FileRestController controller;
	
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileRestController(this.service);
	}
	
	@Test
	public void testNotFound() {
		String id = UUID.randomUUID().toString();
		FileUpdateDTO dto = Mockito.mock(FileUpdateDTO.class);
		
		Mockito.when(this.service.update(dto)).thenReturn(null);
		
		try {
			this.controller.saveFile(id, dto);
			Assert.fail("Save file must throw exception when dto is null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
		}
	}
	
	@Test 
	public void testFound() {
		String id = UUID.randomUUID().toString();
		File file = Mockito.mock(File.class);
		FileUpdateDTO dto = Mockito.mock(FileUpdateDTO.class);
		
		Mockito.when(this.service.update(dto)).thenReturn(file);
		
		try {
			File response = this.controller.saveFile(id, dto);
			Assert.assertSame(file, response);
			Mockito.verify(this.service).update(dto);
		} catch(Exception e) {
			Assert.fail("Save file must not throw exception when dto exists");
		}
	}
}
