package edu.unk.csit406.branding.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.service.FileService;

public class WebCreateUnitTest {

	private String projectId = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileWebController controller;
	private FileService service;
	
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileWebController(this.service);
	}
	
	@Test
	public void testCreate() {
		FileCreateDTO dto = new FileCreateDTO();
		
		dto.setProjectId(this.projectId);		
		String redirectString = this.controller.createFile(dto);
		Assert.assertEquals("redirect:/branding/files/" + this.projectId, redirectString);
		Mockito.verify(this.service).create(dto);
	}
}
