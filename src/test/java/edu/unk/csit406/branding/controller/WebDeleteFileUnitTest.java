package edu.unk.csit406.branding.controller;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class WebDeleteFileUnitTest {
	
	private String id = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileWebController controller;
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.controller = new FileWebController(this.service);
	}
	
	@Test
	public void testGetFileNotFound() {
		Mockito.when(this.service.delete(this.id)).thenReturn(null);
		
		try {
			this.controller.deleteFile(this.id);
			Assert.fail("Must throw exception when entity does not exist");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
		}
	}
	
	@Test
	public void testGetFileHappyPath() {
		FileEntity file = new FileEntity();
		String projectId = UUID.randomUUID().toString();
		
		file.setProjectId(projectId);
		
		Mockito.when(this.service.delete(this.id)).thenReturn(file);
		
		try {
			String template = this.controller.deleteFile(this.id);
			Assert.assertEquals("redirect:/branding/files/" + projectId,template);
			Mockito.verify(this.service).delete(this.id);
					
		} catch(Exception e) {
			Assert.fail("Must not throw exception when entity is found");
		}
		
	}
}
