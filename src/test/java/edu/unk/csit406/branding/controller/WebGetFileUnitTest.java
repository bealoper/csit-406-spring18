package edu.unk.csit406.branding.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;

public class WebGetFileUnitTest {
	private String id = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileWebController controller;
	private Model model;
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.model = Mockito.mock(Model.class);
		this.controller = new FileWebController(this.service);
	}
	
	@Test
	public void testGetFileNotFound() {
		Mockito.when(this.service.get(this.id)).thenReturn(null);
		
		try {
			this.controller.getFile(this.id, this.model);
			Assert.fail("Must throw exception when entity does not exist");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
		}
	}
	
	@Test
	public void testGetFileHappyPath() {
		File file = Mockito.mock(File.class);
		
		Mockito.when(this.service.get(this.id)).thenReturn(file);
		
		try {
			String template = this.controller.getFile(this.id, this.model);
			Assert.assertEquals("branding/files/edit",template);
			Mockito.verify(this.service).get(this.id);
			Mockito.verify(this.model).addAttribute("title","Edit - " + file.getPath());
			Mockito.verify(this.model).addAttribute("file", file);
			
		} catch(Exception e) {
			Assert.fail("Must not throw exception when entity is found");
		}
		
	}
}
