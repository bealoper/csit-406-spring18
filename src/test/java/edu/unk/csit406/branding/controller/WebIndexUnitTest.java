package edu.unk.csit406.branding.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.exception.NotFoundException;
import edu.unk.csit406.branding.service.FileService;
import edu.unk.csit406.validation.ValidationPatterns;

public class WebIndexUnitTest {
	
	private String projectId = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileWebController controller;
	private Model model;
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.model = Mockito.mock(Model.class);
		this.controller = new FileWebController(this.service);
	}
	
	@Test
	public void testIndexNotFound() {
		Mockito.when(this.service.getFileStructure(this.projectId)).thenReturn(null);
		
		try {
			this.controller.index(this.projectId, this.model);
			Assert.fail("Must throw exception when project does not exist");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NotFoundException);
		}
	}
	
	@Test
	public void testIndexHappyPath() {
		FileStructureDTO dto = Mockito.mock(FileStructureDTO.class);
		
		Mockito.when(this.service.getFileStructure(this.projectId)).thenReturn(dto);
		
		try {
			String template = this.controller.index(this.projectId, this.model);
			Assert.assertEquals("branding/files/index",template);
			Mockito.verify(this.service).getFileStructure(this.projectId);
			Mockito.verify(this.model).addAttribute("title","Demo Project Files");
			Mockito.verify(this.model).addAttribute("projectId",this.projectId);
			Mockito.verify(this.model).addAttribute("fileStructure",dto);
			Mockito.verify(this.model).addAttribute("pathPattern",ValidationPatterns.PATH_PATTERN);
		} catch(Exception e) {
			Assert.fail("Must not throw exception when tree is found");
		}
		
	}

}
