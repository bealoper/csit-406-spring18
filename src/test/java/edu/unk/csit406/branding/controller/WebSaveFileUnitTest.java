package edu.unk.csit406.branding.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.service.FileService;

public class WebSaveFileUnitTest {
	private String id = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileWebController controller;
	private Model model;
	private FileService service;
	
	@Before
	public void createMocks() {
		this.service = Mockito.mock(FileService.class);
		this.model = Mockito.mock(Model.class);
		this.controller = new FileWebController(this.service);
	}
	
	@Test
	public void testSaveFileHappyPath() {
		String data = "Hello World";
		FileUpdateDTO dto = new FileUpdateDTO();
		FileEntity file = Mockito.mock(FileEntity.class);
		
		dto.setData(data);
		dto.setId(this.id);
		
		Mockito.when(this.service.saveData(this.id, data)).thenReturn(file);
		
		try {
			String template = this.controller.saveFile(dto, this.model);
			Assert.assertEquals("branding/files/edit",template);
			Mockito.verify(this.service).saveData(this.id,data);
			Mockito.verify(this.model).addAttribute("title","Edit - " + file.getPath());
			Mockito.verify(this.model).addAttribute("file", file);
		} catch(Exception e) {
			Assert.fail("Must not throw exception when tree is found");
		}
		
	}
}
