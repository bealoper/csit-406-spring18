package edu.unk.csit406.branding.repostiory;

import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.repository.FileRepository;

@RunWith(SpringRunner.class)
@DataMongoTest
@AutoConfigureDataMongo
public class FileEntityIntegrationTest {
	
	@Autowired
	private FileRepository repository;

	@Test
	public void testStructure() {
		FileEntity entity = new FileEntity();
		String id = UUID.randomUUID().toString();
		String path = "/js/bundle.min.js";
		String projectId = UUID.randomUUID().toString();
		String data = "alert(\"Hello World\")";
		
		//persist initial entity
		entity.setId(id);
		entity.setPath(path);
		entity.setProjectId(projectId);		
		this.repository.save(entity);
		
		//find then verify both set and automatic field values
		FileEntity persistedentity = this.repository.findOne(id);
		Assert.assertEquals("Saved id must match id", id, persistedentity.getId());
		Assert.assertEquals("Saved path must match path", path, persistedentity.getPath());
		Assert.assertEquals("Saved projectId must match projectId", projectId, persistedentity.getProjectId());
		Assert.assertNotNull("Created date must not be null", persistedentity.getCreatedDate());
		Assert.assertNotNull("Last modified date must not be null", persistedentity.getLastModifiedDate());
		Assert.assertEquals("Version must initialize to zero", 0, (int) (long) persistedentity.getVersion());
		Assert.assertEquals("Data must be an empty array not null", 0, persistedentity.getBytes().length);
		
		//put some data in it and save
		persistedentity.setData(data);
		this.repository.save(persistedentity);
		
		//find then verify version update and data
		persistedentity = this.repository.findOne(id);
		Assert.assertEquals("Version must update from zero to one", 1, (int) (long) persistedentity.getVersion());
		Assert.assertEquals("Data must restore to its original value", data, persistedentity.getData());
		
		//clean up after done
		this.repository.delete(id);
		
		//Verify cleaned up
		Assert.assertNull(this.repository.findOne(id));
	}
	
	@Test
	public void testFindByProjectId() {
		String projectId = UUID.randomUUID().toString();
		String entityOnePath = "/js/bundle.min.js";
		String entityTwoPath = "/css/styles.css";
	
		//persist entity one
		FileEntity entityOne = new FileEntity();
		entityOne.setId(UUID.randomUUID().toString());
		entityOne.setPath(entityOnePath);
		entityOne.setProjectId(projectId);		
		this.repository.save(entityOne);
		
		//persist entity two
		FileEntity entityTwo = new FileEntity();
		entityTwo.setId(UUID.randomUUID().toString());
		entityTwo.setPath(entityTwoPath);
		entityTwo.setProjectId(projectId);		
		this.repository.save(entityTwo);
		
		//persist entity three (control entity)
		FileEntity entityThree = new FileEntity();
		entityThree.setId(UUID.randomUUID().toString());
		entityThree.setPath("/components/elements.js");
		entityThree.setProjectId(UUID.randomUUID().toString());		
		this.repository.save(entityThree);
		
		//test results
		List<FileEntity> entitys = this.repository.findByProjectIdOrderByPathAsc(projectId);
		Assert.assertEquals(2, entitys.size());
		
		FileEntity entity = entitys.get(0);
		Assert.assertEquals("entity two path must be index zero", entityTwoPath, entity.getPath());
		entity = entitys.get(1);
		Assert.assertEquals("Modle one path must be index one", entityOnePath, entity.getPath());
		
		//clean up when done
		this.repository.delete(entitys);
		this.repository.delete(entityThree);
	}
	
	@Test
	public void testCustomUpdateMethod() throws InterruptedException {
		String id = UUID.randomUUID().toString();
		String projectId = UUID.randomUUID().toString();
		String originalPath = "/js/bundle.min.js";
		String updatePath = "/public/js/bundle.min.js";
		String originalData = "let data = 'Hello World';";
		String updateData = "let data = 'Hello Kearney';";
	
		//persist entity
		FileEntity entity = new FileEntity();
		entity.setId(id);
		entity.setPath(originalPath);
		entity.setProjectId(projectId);	
		entity.setData(originalData);		
		this.repository.save(entity);
		
		//get persisted entity
		FileEntity persistedentity = this.repository.findOne(id);
		Assert.assertEquals(originalPath, persistedentity.getPath());
		Assert.assertEquals(originalData, persistedentity.getData());
		
		//update entity		
		persistedentity.setProjectId(UUID.randomUUID().toString());
		persistedentity.setPath(updatePath);
		persistedentity.setData(updateData);
		Assert.assertNotNull(this.repository.updateFileEntity(persistedentity));
		
		
		//get updated entity
		FileEntity updatedentity = this.repository.findOne(id);
		Assert.assertEquals("It should not be possible to change the projectId", projectId, updatedentity.getProjectId());
		Assert.assertNotNull("Updating must not null created data", updatedentity.getCreatedDate());
		Assert.assertNotEquals("Last modified date must change", persistedentity.getLastModifiedDate(), updatedentity.getLastModifiedDate());
		Assert.assertEquals("The version number must increment by one", ((int) (long) persistedentity.getVersion() + 1), ((int) (long) updatedentity.getVersion()));
		Assert.assertEquals("The path must update", updatePath, updatedentity.getPath());
		Assert.assertEquals("The data must update", updateData, updatedentity.getData());
		
		//clean up when done
		this.repository.delete(id);
	}
}