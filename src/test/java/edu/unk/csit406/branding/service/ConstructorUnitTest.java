package edu.unk.csit406.branding.service;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.repository.FileRepository;

public class ConstructorUnitTest {

	@Test
	public void testConstructor() {
		Validator validator = Mockito.mock(Validator.class);
		FileRepository repository =  Mockito.mock(FileRepository.class);
		FileService service;
		
		try {
			service = new FileServiceImpl(repository, null);
			Assert.fail("Constructor must throw if validator null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NullPointerException);
		}
		
		try {
			service = new FileServiceImpl(null, validator);
			Assert.fail("Constructor must throw if repository null");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof NullPointerException);
		}
		
		try {
			service = new FileServiceImpl(repository, validator);
			Assert.assertNotNull(service);
		} catch(Exception e) {
			Assert.fail("Constructor must throw if validator null");
		}
		
	}
}
