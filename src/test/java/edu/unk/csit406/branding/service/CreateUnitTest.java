package edu.unk.csit406.branding.service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.dto.FileCreateDTO;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.entity.MimeType;
import edu.unk.csit406.branding.repository.FileRepository;


public class CreateUnitTest {
	
	private FileRepository repository;
	private FileService service;
	private Validator validator;

	@Before
	public void createMocks() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}
	
	@Test
	public void testHappyPath() {
		FileCreateDTO dtoOrig = new FileCreateDTO();
		dtoOrig.setProjectId(UUID.randomUUID().toString());
		dtoOrig.setPath("/js/jquery.js");
		
		Set<ConstraintViolation<FileCreateDTO>> violations = new HashSet<ConstraintViolation<FileCreateDTO>>();
		Mockito.when(this.validator.validate(dtoOrig)).thenReturn(violations);
		Mockito.when(this.repository.exists(Mockito.anyString())).thenReturn(false);
		Mockito.when(this.repository.save(Mockito.any(FileEntity.class))).then(new Answer<FileEntity>() {

			@Override
			public FileEntity answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
			      return (FileEntity) args[0];
			}
			
		});
		
		File fileNew = this.service.create(dtoOrig);
		Mockito.verify(this.validator).validate(dtoOrig);
		Mockito.verify(this.repository).exists(Mockito.anyString());
		Mockito.verify(this.repository).save(Mockito.any(FileEntity.class));
		
		Assert.assertNotNull(fileNew);
		Assert.assertNotNull(fileNew.getId());
		try {
			UUID.fromString(fileNew.getId());
		} catch(Exception e) {
			Assert.fail("ID must be a valid UUID");
		}
		
		Assert.assertEquals(dtoOrig.getProjectId(), fileNew.getProjectId());
		Assert.assertEquals(dtoOrig.getPath(), fileNew.getPath());
		Assert.assertEquals(MimeType.JS, fileNew.getType());
	}
	
	@Test
	public void testInvalidInput() {
		FileCreateDTO dtoOrig = new FileCreateDTO();
		dtoOrig.setProjectId(UUID.randomUUID().toString());
		dtoOrig.setPath("/js/jquery.js");
		
		@SuppressWarnings("unchecked")
		ConstraintViolation<FileCreateDTO> violation = (ConstraintViolation<FileCreateDTO>) Mockito.mock(ConstraintViolation.class);
		Set<ConstraintViolation<FileCreateDTO>> violations = new HashSet<ConstraintViolation<FileCreateDTO>>();
		violations.add(violation);
		Mockito.when(this.validator.validate(dtoOrig)).thenReturn(violations);
		
		try {
			this.service.create(dtoOrig);
			Assert.fail("Create must throw exception when dto is invalid");
		} catch(Exception e) {
			Assert.assertTrue(e instanceof ConstraintViolationException);
		}
	}
	
	@Test
	public void testIdAlredyExists() {
		FileCreateDTO dtoOrig = new FileCreateDTO();
		dtoOrig.setProjectId(UUID.randomUUID().toString());
		dtoOrig.setPath("/js/jquery.js");
		
		Set<ConstraintViolation<FileCreateDTO>> violations = new HashSet<ConstraintViolation<FileCreateDTO>>();
		Mockito.when(this.validator.validate(dtoOrig)).thenReturn(violations);
		Mockito.when(this.repository.exists(Mockito.anyString())).then(new Answer<Boolean>() {
			private int count = 0;
			
			@Override
			public Boolean answer(InvocationOnMock invocation) throws Throwable {
				if(count < 3) {
					count++;
					return true;
				}
				return false;
			}
		});
		Mockito.when(this.repository.save(Mockito.any(FileEntity.class))).thenReturn(null);
		
		
		this.service.create(dtoOrig);
		Mockito.verify(this.repository, Mockito.times(4)).exists(Mockito.anyString());
	}
	
	@Test
	public void testUnknownMimeType() {
		FileCreateDTO dtoOrig = new FileCreateDTO();
		dtoOrig.setProjectId(UUID.randomUUID().toString());
		dtoOrig.setPath("/js/jquery.abc");
		
		Set<ConstraintViolation<FileCreateDTO>> violations = new HashSet<ConstraintViolation<FileCreateDTO>>();
		Mockito.when(this.validator.validate(dtoOrig)).thenReturn(violations);
		Mockito.when(this.repository.exists(Mockito.anyString())).thenReturn(false);
		Mockito.when(this.repository.save(Mockito.any(FileEntity.class))).then(new Answer<FileEntity>() {

			@Override
			public FileEntity answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
			      return (FileEntity) args[0];
			}
			
		});
		
		File fileNew = this.service.create(dtoOrig);
		Mockito.verify(this.validator).validate(dtoOrig);
		Mockito.verify(this.repository).exists(Mockito.anyString());
		Mockito.verify(this.repository).save(Mockito.any(FileEntity.class));
		
		Assert.assertNotNull(fileNew);
		Assert.assertNotNull(fileNew.getId());
		try {
			UUID.fromString(fileNew.getId());
		} catch(Exception e) {
			Assert.fail("ID must be a valid UUID");
		}
		
		Assert.assertEquals(dtoOrig.getProjectId(), fileNew.getProjectId());
		Assert.assertEquals(dtoOrig.getPath(), fileNew.getPath());
		Assert.assertEquals(MimeType.TEXT, fileNew.getType());
	}
}



























