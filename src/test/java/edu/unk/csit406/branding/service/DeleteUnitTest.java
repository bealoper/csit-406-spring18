package edu.unk.csit406.branding.service;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.repository.FileRepository;


public class DeleteUnitTest {

	private String id = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileRepository repository;
	private Validator validator;	
	private FileService service;
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}
	
	@Test
	public void testDeleteFailException() {
		FileEntity file = new FileEntity();
		Mockito.when(this.repository.findOne(this.id)).thenReturn(file);
		Mockito.doThrow(new IllegalArgumentException()).when(this.repository).delete(this.id);
		
		File deleted = this.service.delete(this.id);
		Assert.assertNull("Delete must return null when exception thrown",deleted);
		Mockito.verify(this.repository).findOne(this.id);
		Mockito.verify(this.repository).delete(this.id);
	}
	
	@Test
	public void testDeleteFailNotFound() {
		Mockito.when(this.repository.findOne(this.id)).thenReturn(null);
		
		File deleted = this.service.delete(this.id);
		Assert.assertNull("Delete must return null when entity not found",deleted);
		Mockito.verify(this.repository).findOne(this.id);
	}
	
	@Test
	public void testDeleteSuccess() {
		FileEntity file = new FileEntity();
		Mockito.when(this.repository.findOne(this.id)).thenReturn(file);
		
		File deleted = this.service.delete(this.id);
		Assert.assertSame("Delete must return deleted entity when exception not thrown",file, deleted);
		Mockito.verify(this.repository).findOne(this.id);
		Mockito.verify(this.repository).delete(this.id);
	}
}
