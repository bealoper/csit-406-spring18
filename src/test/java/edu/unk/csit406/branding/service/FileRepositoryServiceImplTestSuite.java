package edu.unk.csit406.branding.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ConstructorUnitTest.class,
	CreateUnitTest.class, 
	DeleteUnitTest.class,
	GetUnitTest.class,
	GetTreeStructureTest.class,
	RenameUnitTest.class,
	SaveDataUnitTest.class,
	UpdateUnitTest.class
})
public class FileRepositoryServiceImplTestSuite {

}
