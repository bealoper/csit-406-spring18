package edu.unk.csit406.branding.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.FileStructureDTO;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.repository.FileRepository;

public class GetTreeStructureTest {
	
	private String projectId = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private static List<FileEntity> dataList;
	private FileRepository repository;
	private Validator validator;	
	private FileService service;
	
	@BeforeClass
	public static void readTestFile() throws IOException, URISyntaxException {
		List<String> lines = Files.readAllLines(Paths.get(ClassLoader.getSystemResource("tree_loader_test_data.txt").toURI()));
		
		GetTreeStructureTest.dataList = new ArrayList<FileEntity>();
		
		for(String line : lines) {	
			FileEntity entity = new FileEntity();
			entity.setId(UUID.randomUUID().toString());
			entity.setPath(line);
			GetTreeStructureTest.dataList.add(entity);
		}
	}
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}

	@Test
	public void testHappyPath() {
		Mockito.when(this.repository.findByProjectIdOrderByPathAsc(projectId)).thenReturn(dataList);
		
		FileStructureDTO tree = this.service.getFileStructure(projectId);
		
		Mockito.verify(this.repository).findByProjectIdOrderByPathAsc(projectId);
		Assert.assertEquals(4, tree.getChildren().size());
		Assert.assertEquals("/", tree.getPath());
		Assert.assertEquals("root", tree.getName());
	}
	
	@Test
	public void testNullProjectId() {
		try {
			this.service.getFileStructure(null);
			Assert.fail("Get file structure must throw exception when passed a null project id");
		} catch(Exception e) {
			Assert.assertTrue("Get file structure must throw NullPointerException when passed a null project id", e instanceof NullPointerException);
		}
	}
}
