package edu.unk.csit406.branding.service;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.repository.FileRepository;

public class GetUnitTest {

	private String id = "4e645e30-31b6-4afa-88f6-67cba676f215";
	private FileRepository repository;
	private Validator validator;	
	private FileService service;
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}
	
	@Test
	public void testGetFailure() {
		Mockito.when(this.repository.findOne(this.id)).thenReturn(null);
		
		File file = this.service.get(this.id);
		Assert.assertNull(file);
		Mockito.verify(this.repository).findOne(this.id);
	}
	
	@Test
	public void testGetSuccess() {
		FileEntity entity = new FileEntity();
		
		Mockito.when(this.repository.findOne(this.id)).thenReturn(entity);
		
		File file = this.service.get(this.id);
		Assert.assertSame(entity, file);
		Mockito.verify(this.repository).findOne(this.id);
	}
}
