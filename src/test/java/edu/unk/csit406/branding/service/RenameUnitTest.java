package edu.unk.csit406.branding.service;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.repository.FileRepository;

public class RenameUnitTest {

	private FileRepository repository;
	private Validator validator;
	private FileService service;
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}
	
	@Test
	public void testRenameFailure() {
		try {
			this.service.rename(null, "/js/bundle.js");
			Assert.fail("Rename must throw exception when null id passed");
		} catch(Exception e) {
			Assert.assertTrue("Rename must throw NullPointerException when id is null", e instanceof NullPointerException);
		}
		
		try {
			this.service.rename("4e645e30-31b6-4afa-88f6-67cba676f215", null);
			Assert.fail("Rename must throw exception when null path passed");
		} catch(Exception e) {
			Assert.assertTrue("Rename must throw NullPointerException when path is null", e instanceof NullPointerException);
		}
	}
}
