package edu.unk.csit406.branding.service;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.repository.FileRepository;

public class SaveDataUnitTest {
	private FileRepository repository;
	private Validator validator;	
	private FileService service;
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
	}
	
	@Test
	public void testSaveDataFailure() {
		try {
			this.service.saveData(null, "alert(\"Hello World\");");
			Assert.fail("saveData must throw exception when null id passed");
		} catch(Exception e) {
			Assert.assertTrue("saveData must throw NullPointerException when id is null", e instanceof NullPointerException);
		}
		
		try {
			this.service.rename("4e645e30-31b6-4afa-88f6-67cba676f215", null);
			Assert.fail("saveData must throw exception when null data passed");
		} catch(Exception e) {
			Assert.assertTrue("saveData must throw NullPointerException when data is null", e instanceof NullPointerException);
		}
	}
}
