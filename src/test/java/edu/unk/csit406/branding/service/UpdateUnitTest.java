package edu.unk.csit406.branding.service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import edu.unk.csit406.branding.dto.File;
import edu.unk.csit406.branding.dto.FileUpdateDTO;
import edu.unk.csit406.branding.entity.FileEntity;
import edu.unk.csit406.branding.repository.FileRepository;

public class UpdateUnitTest {
	
	private FileRepository repository;
	private Validator validator;	
	private FileService service;
	private Set<ConstraintViolation<FileUpdateDTO>> violations;
	
	@Before
	public void setUp() {
		this.repository = Mockito.mock(FileRepository.class);
		this.validator = Mockito.mock(Validator.class);
		this.service = new FileServiceImpl(this.repository, this.validator);
		this.violations = new HashSet<ConstraintViolation<FileUpdateDTO>>();
	}
	
	@Test
	public void testHappyPath() {
		FileUpdateDTO dto = new FileUpdateDTO();
		FileEntity entity = new FileEntity();
		String id = UUID.randomUUID().toString();
		String pathOriginal = "/js/bundle.js";
		String pathChanged = "/public/js/bundle.js";
		String dataOrginal = "alert(\"Hello World\");";
		String dataChanged = "alert(\"Hello Kearney\");";
		
		dto.setId(id);
		dto.setPath(pathChanged);
		dto.setData(dataChanged);
		
		entity.setId(id);
		entity.setPath(pathOriginal);
		entity.setData(dataOrginal);
		
		Mockito.when(this.validator.validate(dto)).thenReturn(this.violations);
		Mockito.when(this.repository.findOne(id)).thenReturn(entity);
		Mockito.when(this.repository.save(entity)).then(new Answer<FileEntity>() {

			@Override
			public FileEntity answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
			      return (FileEntity) args[0];
			}
			
		});
		
		
		File updatedFile = this.service.update(dto);
		Mockito.verify(this.validator).validate(dto);
		Mockito.verify(this.repository).findOne(id);
		Mockito.verify(this.repository).save(entity);
		Assert.assertNotNull(updatedFile);
		Assert.assertEquals(id, updatedFile.getId());
		Assert.assertEquals(pathChanged, updatedFile.getPath());
		Assert.assertEquals(dataChanged, updatedFile.getData());
	}
	
	@Test
	public void testNullArgumentZerio() {
		try {
			this.service.update(null);
			Assert.fail("Update must throw an exception when passed a null argument");
		} catch(Exception e) {
			Assert.assertTrue("Update must throw NullPointerException when dto is null", e instanceof NullPointerException);
		}
	}
	
	@Test
	public void testInvalidArgumentZero() {
		FileUpdateDTO dto = new FileUpdateDTO();
		
		@SuppressWarnings("unchecked")
		ConstraintViolation<FileUpdateDTO> violation = (ConstraintViolation<FileUpdateDTO>) Mockito.mock(ConstraintViolation.class);
		violations.add(violation);
		
		Mockito.when(this.validator.validate(dto)).thenReturn(this.violations);
		
		try {
			this.service.update(dto);
			Assert.fail("Update must throw an exception when passed an invalid argument");
		} catch(Exception e) {
			Assert.assertTrue("Update must throw ConstraintViolationException when dto is invalid", e instanceof ConstraintViolationException);
			
			Mockito.verify(this.validator).validate(dto);
			
			ConstraintViolationException castE = (ConstraintViolationException) e;
			Assert.assertSame("ConstraintViolationException must contain violations", violation, castE.getConstraintViolations().iterator().next());			
		}
	}
	
	@Test
	public void testFailToInsert() {
		FileUpdateDTO dto = new FileUpdateDTO();
		
		Mockito.when(this.validator.validate(dto)).thenReturn(this.violations);
		Mockito.when(this.repository.findOne(Mockito.anyString())).thenReturn(null);
		
		File updatedFile = this.service.update(dto);
		Assert.assertNull("Update must return null when the database fails to find the entity", updatedFile);
		Mockito.verify(this.validator).validate(dto);
		Mockito.verify(this.repository).findOne(Mockito.anyString());
	}
}
