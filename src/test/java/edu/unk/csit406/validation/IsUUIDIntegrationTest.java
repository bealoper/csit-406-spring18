package edu.unk.csit406.validation;

import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class IsUUIDIntegrationTest {

	private Validator validator;
	
	private class TestEntity {
		
		@IsUUID
		public String id;
	}
	
	@Before
	public void createValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
	}
	
	@Test
	public void testNullValue() {
		TestEntity entity = new TestEntity();
		
		Set<ConstraintViolation<TestEntity>> violations = this.validator.validate(entity);
		Assert.assertEquals(0, violations.size());
		
	}
	
	@Test
	public void testValidValue() {
		TestEntity entity = new TestEntity();
		
		entity.id = UUID.randomUUID().toString();
		
		Set<ConstraintViolation<TestEntity>> violations = this.validator.validate(entity);
		Assert.assertEquals(0, violations.size());
		
	}
	
	@Test
	public void testInvalidValue() {
		TestEntity entity = new TestEntity();
		
		entity.id = "I am an invalid UUID";
		
		Set<ConstraintViolation<TestEntity>> violations = this.validator.validate(entity);
		Assert.assertEquals(1, violations.size());
		
	}
	
}
