package edu.unk.csit406.validation;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UuidValidatorUnitTest {
	
	private UuidValidator validator;
	
	@Before
	public void createValidator() {
		this.validator = new UuidValidator();
	}
	

	@Test
	public void testNullValue() {
		Assert.assertTrue(this.validator.isValid(null, null));
	}
	
	@Test
	public void testvalidValue() {
		Assert.assertTrue(this.validator.isValid(UUID.randomUUID().toString(), null));
	}
	
	@Test
	public void testInvalidValue() {
		Assert.assertFalse(this.validator.isValid("Test test test", null));
	}
}
