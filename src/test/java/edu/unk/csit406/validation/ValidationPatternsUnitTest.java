package edu.unk.csit406.validation;

import org.junit.Assert;
import org.junit.Test;

public class ValidationPatternsUnitTest {
	
	@Test
	public void testPathExpression() {
		Assert.assertFalse("Root Empty File Path", "/".matches(ValidationPatterns.PATH_PATTERN));
		Assert.assertFalse("No Filename", "/public/".matches(ValidationPatterns.PATH_PATTERN));
		Assert.assertTrue("Root Hidden File Path", "/.gitignore".matches(ValidationPatterns.PATH_PATTERN));
		Assert.assertTrue("Root Regular File Path", "/pom.xml".matches(ValidationPatterns.PATH_PATTERN));
		Assert.assertTrue("Deep Regular File Path", "/public/js/jquery-2.0.min.js".matches(ValidationPatterns.PATH_PATTERN));
		Assert.assertTrue("Deep Path Special Char", "/public_test-case/my_script-1.js".matches(ValidationPatterns.PATH_PATTERN));	
	}
}
