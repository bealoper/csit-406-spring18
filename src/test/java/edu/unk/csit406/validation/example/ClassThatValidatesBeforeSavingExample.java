package edu.unk.csit406.validation.example;

import java.util.Objects;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import edu.unk.csit406.branding.entity.FileEntity;

public class ClassThatValidatesBeforeSavingExample {
	
	final private Validator validator;
	
	public ClassThatValidatesBeforeSavingExample(Validator validator) {
		this.validator = Objects.requireNonNull(validator);
	}
	
	
	public FileEntity validateAndSave(FileEntity entity) {
		Set<ConstraintViolation<FileEntity>> violations = this.validator.validate(entity);
		
		if(violations.size() > 0) {
			throw new ConstraintViolationException("FileEntity is invalid", violations);
		}
		
		//do save logic here
		
		return entity;
	}
}
