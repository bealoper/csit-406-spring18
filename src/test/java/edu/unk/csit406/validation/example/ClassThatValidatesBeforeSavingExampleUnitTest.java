package edu.unk.csit406.validation.example;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.unk.csit406.branding.entity.FileEntity;

public class ClassThatValidatesBeforeSavingExampleUnitTest {
	
	private ClassThatValidatesBeforeSavingExample testedClass;
	private Validator validator;

	@Before
	public void setupValidator() {
		/*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();*/
		
		this.validator = Mockito.mock(Validator.class);
		this.testedClass = new ClassThatValidatesBeforeSavingExample(this.validator);
	}
	
	@Test
	public void testHappyPathValidator() {
		FileEntity entity = Mockito.mock(FileEntity.class);
		Set<ConstraintViolation<FileEntity>> violations = new HashSet<ConstraintViolation<FileEntity>>();
		
		Mockito.when(this.validator.validate(entity)).thenReturn(violations);
	
		try {
			this.testedClass.validateAndSave(entity);
			Mockito.verify(this.validator).validate(entity);
			
		} catch(Exception e) {
			Assert.fail("method must not throw exception");
		}
	}
	
	@Test
	public void testValidationException() {
		FileEntity entity = Mockito.mock(FileEntity.class);
		Set<ConstraintViolation<FileEntity>> violations = new HashSet<ConstraintViolation<FileEntity>>();
		
		@SuppressWarnings("unchecked")
		ConstraintViolation<FileEntity> violation = (ConstraintViolation<FileEntity>) Mockito.mock(ConstraintViolation.class);
		
		violations.add(violation);
		Mockito.when(this.validator.validate(entity)).thenReturn(violations);
	
		try {
			this.testedClass.validateAndSave(entity);
			Assert.fail("method must throw exception");
		} catch(Exception e) {
			Mockito.verify(this.validator).validate(entity);
			Assert.assertTrue(e instanceof ConstraintViolationException);
		}
	}
}
